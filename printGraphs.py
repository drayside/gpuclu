import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


prefetch = open("prefetch_log_stats.txt", "r")
line = prefetch.read()
prefetch_latency = float(line.split()[1])
prefetch_compute = float(line.split()[5])
prefetch.close()

no_prefetch = open("no_prefetch_log_stats.txt", "r")
line = no_prefetch.read()
no_prefetch_latency = float(line.split()[1])
no_prefetch_compute = float(line.split()[5])
no_prefetch.close()

packed_sum = open("packed_sum_log_stats.txt", "r")
line = packed_sum.read()
packed_sum_latency = float(line.split()[1])
packed_sum_compute = float(line.split()[5])
packed_sum.close()

unpacked_sum = open("unpacked_sum_log_stats.txt", "r")
line = unpacked_sum.read()
unpacked_sum_latency = float(line.split()[1])
unpacked_sum_compute = float(line.split()[5])
unpacked_sum.close()

packed_sum_byte = open("packed_sum_byte_log_stats.txt", "r")
line = packed_sum_byte.read()
packed_sum_byte_latency = float(line.split()[1])
packed_sum_byte_compute = float(line.split()[5])
packed_sum_byte.close()

unpacked_sum_byte = open("unpacked_sum_byte_log_stats.txt", "r")
line = unpacked_sum_byte.read()
unpacked_sum_byte_latency = float(line.split()[1])
unpacked_sum_byte_compute = float(line.split()[5])
unpacked_sum_byte.close()

matrix_copy_xy = open("matrix_copy_xy_log_stats.txt", "r")
line = matrix_copy_xy.read()
matrix_copy_xy_latency = float(line.split()[1])
matrix_copy_xy_compute = float(line.split()[5])
matrix_copy_xy.close()

matrix_copy_yx = open("matrix_copy_yx_log_stats.txt", "r")
line = matrix_copy_yx.read()
matrix_copy_yx_latency = float(line.split()[1])
matrix_copy_yx_compute = float(line.split()[5])
matrix_copy_yx.close()

N = 1
width = 0.1

labels = ("Prefetching OFF", "Prefetching ON")

series1color = 'c'
series2color = 'b'

p1 = plt.bar(0.5, no_prefetch_latency, width, color=series1color)
p2 = plt.bar(0.7, prefetch_latency, width,    color=series2color)
plt.title("Upload latencies")
plt.ylabel("Time (ms)")
plt.xticks((0.55, 0.75), labels)

plt.savefig("prefetch_latency.png")
plt.close()

p1 = plt.bar(0.5, no_prefetch_compute, width, color=series1color)
p2 = plt.bar(0.7, prefetch_compute, width,    color=series2color)
plt.title("Kernel compute times")
plt.ylabel("Time (ms)")
plt.xticks((0.55, 0.75), labels)

plt.savefig("prefetch_compute.png")
plt.close()

labels = ("Packing OFF", "Packing ON")

p1 = plt.bar(0.5, unpacked_sum_latency, width, color=series1color)
p2 = plt.bar(0.7, packed_sum_latency, width,    color=series2color)
plt.title("Upload latencies")
plt.ylabel("Time (ms)")
plt.xticks((0.55, 0.75), labels)

plt.savefig("sum_latency.png")
plt.close()

p1 = plt.bar(0.5, unpacked_sum_compute, width, color=series1color)
p2 = plt.bar(0.7, packed_sum_compute, width,    color=series2color)
plt.title("Kernel compute times")
plt.ylabel("Time (ms)")
plt.xticks((0.55, 0.75), labels)

plt.savefig("sum_compute.png")
plt.close()

labels = ("Packing OFF", "Packing ON")

p1 = plt.bar(0.5, unpacked_sum_byte_latency, width, color=series1color)
p2 = plt.bar(0.7, packed_sum_byte_latency, width,    color=series2color)
plt.title("Upload latencies")
plt.ylabel("Time (ms)")
plt.xticks((0.55, 0.75), labels)

plt.savefig("sum_byte_latency.png")
plt.close()

p1 = plt.bar(0.5, unpacked_sum_byte_compute, width, color=series1color)
p2 = plt.bar(0.7, packed_sum_byte_compute, width, color=series2color)
plt.title("Kernel compute times")
plt.ylabel("Time (ms)")
plt.xticks((0.55, 0.75), labels)

plt.savefig("sum_byte_compute.png")
plt.close()

labels = ("Work dimensions: XY", "Work dimensions: YX")

p1 = plt.bar(0.5, matrix_copy_xy_latency, width, color=series1color)
p2 = plt.bar(0.7, matrix_copy_yx_latency, width, color=series2color)
plt.title("Upload latencies")
plt.ylabel("Time (ms)")
plt.xticks((0.55, 0.75), labels)

plt.savefig("matrix_latency.png")
plt.close()

p1 = plt.bar(0.5, matrix_copy_xy_compute, width, color=series1color)
p2 = plt.bar(0.7, matrix_copy_yx_compute, width, color=series2color)
plt.title("Kernel compute times")
plt.ylabel("Time (ms)")
plt.xticks((0.55, 0.75), labels)

plt.savefig("matrix_compute.png")
plt.close()

slow_latencies = (matrix_copy_xy_latency, unpacked_sum_byte_latency, unpacked_sum_latency, no_prefetch_latency)
fast_latencies = (matrix_copy_yx_latency, packed_sum_byte_latency, packed_sum_latency, prefetch_latency) 
slow_computes = (matrix_copy_xy_compute, unpacked_sum_byte_compute, unpacked_sum_compute, no_prefetch_compute)
fast_computes = (matrix_copy_yx_compute, packed_sum_byte_compute, packed_sum_compute, prefetch_compute) 

N = 4
width = 0.3
ind = np.arange(N)

labels = ("Dimensions: YX", "Dimensions: XY", "Byte-Packing: ON", "Byte-Packing: OFF", 
			"2-bit Packing: ON", "2-bit Packing: OFF", "Prefetching: ON", "Prefetching: OFF")

indices = (0.15, 0.45, 1.15, 1.45, 2.15, 2.45, 3.15, 3.45)

p1 = plt.barh(ind, fast_latencies, width, color=series2color)
p2 = plt.barh(ind + width, slow_latencies, width, color=series1color)
plt.title("Upload latencies")
plt.xlabel("Time (ms)")
plt.yticks(indices, labels)
plt.subplots_adjust(left=0.2)

plt.savefig("overall_latencies.png")
plt.close()

p1 = plt.barh(ind, fast_computes, width, color=series2color)
p2 = plt.barh(ind + width, slow_computes, width, color=series1color)
plt.title("Kernel compute times")
plt.xlabel("Time (ms)")
plt.yticks(indices, labels)
plt.subplots_adjust(left=0.2)

plt.savefig("overall_computes.png")
plt.close()







