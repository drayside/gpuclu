package ca.uwaterloo.gpuclu;

import java.util.logging.Logger;

import org.jocl.Sizeof;
import org.jocl.cl_event;
import org.junit.Test;

public class TestSimpleGPU {

  @Test
  public void testSimpleGPU() {
    final PackedArrayImpl a = new PackedArrayImpl(2, 1);
    final TestSimpleComputation c = new TestSimpleComputation(a);
    final OpenCLComputer computer = new OpenCLComputer(true, true, c);
    computer.initialize();
    c.inputMem().h_append(2);
    computer.compute();
    computer.release();
  }

  @Test
  public void testSimpleGPU2() {
    final PackedArrayImpl a = new PackedArrayImpl(2, 10);
    final TestSimpleComputation c = new TestSimpleComputation(a);
    final OpenCLComputer computer = new OpenCLComputer(true, true, c);
    computer.initialize();
    c.inputMem().h_append(2);
    c.inputMem().h_append(3);
    c.inputMem().h_append(1);
    c.inputMem().h_append(1);
    computer.compute();
    c.inputMem().h_append(2);
    c.inputMem().h_append(3);
    computer.compute();
    computer.release();
  }

  class TestSimpleComputation implements OpenCLComputation {

    final static boolean prefetch = false;
    final static int workgroupSize = 32;

    private final static String kernel_file_copy_int = "src/ca/uwaterloo/gpuclu/copy_int.cl";
    private final static String kernel_file_test_packed = "test/ca/uwaterloo/gpuclu/test_packed.cl";

    final PackedArrayImpl p;

    final Mem[] mems;
    final Dim[] dims;

    final MemPacked input;
    final MemPacked packed_output1;
    final MemPacked packed_output2;
    final MemInt unpacked_output;

    TestSimpleComputation(final PackedArrayImpl p) {
      this.p = p;
      this.input =
          new MemPacked("input", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> p.cellCount()), p.config.bitwidth, prefetch, workgroupSize, p.cellCount());
      this.packed_output1 =
          new MemPacked("packed_output1", MemConfig.DownloadOnly, new MemCapacity(
              () -> p.cellCount()), p.config.bitwidth, prefetch, workgroupSize, p.cellCount());
      this.packed_output2 =
          new MemPacked("packed_output2", MemConfig.DownloadOnly, new MemCapacity(
              () -> p.cellCount()), p.config.bitwidth, prefetch, workgroupSize, p.cellCount());
      this.unpacked_output =
          new MemInt("unpacked_output", MemConfig.DownloadOnly, new MemCapacity(() -> p.capacity()));
      this.mems = new Mem[] {input, packed_output1, packed_output2, unpacked_output};
      this.dims = new Dim[] {new Dim(0, input.capacity.deviceCapacity())};
    }

    public MemPacked inputMem() {
      return input;
    }

    @Override
    public Mem[] memories() {
      return mems;
    }

    @Override
    public Dim[] dimensions() {
      return dims;
    }

    @Override
    public Kernel[] kernels() {
      return kernels;
    }

    final Kernel[] kernels = new Kernel[] {new Kernel(kernel_file_copy_int, "copy_int") {

      @Override
      public cl_event prepareLaunchInner() {
        setArg("source", Sizeof.cl_mem, input.d_pointer());
        setArg("target", Sizeof.cl_mem, packed_output1.d_pointer());
        setArg("length", Sizeof.cl_int, OpenCLUtil.pointerToInt(input.capacity.deviceCapacity()));

        // launch copy_int kernel
        final cl_event kernel_execution_event = new cl_event();
        final int work_items = dims[0].size;// input.capacity.deviceCapacity();
        total_work_items += work_items;

        work_dim = dims.length;
        global_work_offset = null;
        global_work_size = new long[] {work_items};
        local_work_size = null;
        num_events_in_wait_list = 0;
        event_wait_list = null;
        event = kernel_execution_event;

        return kernel_execution_event;
      }
    }, new Kernel(kernel_file_test_packed, "test_packed") {

      @Override
      public cl_event prepareLaunchInner() {
        setArg(input);
        setArg(packed_output2);
        setArg(unpacked_output);

        // launch test_packed kernel
        final cl_event kernel_execution_event = new cl_event();
        final int work_items = input.logicalCapacity();
        total_work_items += work_items;

        work_dim = dims.length;
        global_work_offset = null;
        global_work_size = new long[] {work_items};
        local_work_size = null;
        num_events_in_wait_list = 0;
        event_wait_list = null;
        event = kernel_execution_event;

        return kernel_execution_event;
      }
    }};


    @Override
    public void uploadPreprocessing() {}

    @Override
    public void uploadPostprocessing() {}

    @Override
    public void downloadPreprocessing() {}

    @Override
    public void downloadPostprocessing() {
      assert input.h.capacity() == packed_output1.h.capacity() : "input/output capacities don't match: "
          + Util.i2s(input.h.capacity(), packed_output1.h.capacity());
      assert input.h.capacity() == packed_output2.h.capacity() : "input/output capacities don't match: "
          + Util.i2s(input.h.capacity(), packed_output2.h.capacity());


      assert input.h.size() == packed_output1.h.size() : "input/output sizes don't match: "
          + Util.i2s(input.h.size(), packed_output1.h.size());
      assert input.h.size() == packed_output2.h.size() : "input/output sizes don't match: "
          + Util.i2s(input.h.size(), packed_output2.h.size());
      assert input.h.capacity() == unpacked_output.h_length() : "input/output capacity/size doesn't match: "
          + Util.i2s(input.h.capacity(), unpacked_output.h_length());

      for (int i = 0; i < unpacked_output.h_length(); i++) {
        final int x = unpacked_output.h[i];
        final int y = input.h.get(i);
        final int z = packed_output1.h.get(i);
        final int z2 = packed_output2.h.get(i);
        assert x == y : "values differ at index " + i;
        assert x == z : "values differ at index " + i;
        assert z == z2 : "values differ at index " + i;
      }
    }

    @Override
    public Logger logger() {
      return logger;
    }

    private final Logger logger = Logger.getLogger(TestSimpleGPU.class.getSimpleName());

  }
}
