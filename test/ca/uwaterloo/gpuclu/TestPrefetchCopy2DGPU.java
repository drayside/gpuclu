package ca.uwaterloo.gpuclu;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Random;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;

import org.jocl.Sizeof;
import org.jocl.cl_event;
import org.junit.Test;

public class TestPrefetchCopy2DGPU {

  private static final int SIZE = 1024;

  @Test
  public void testPrefetchCopyGPU() {
    final PackedArrayImpl a = new PackedArrayImpl(4, SIZE);
    final OpenCLComputation c = new TestPrefetchCopyComputation(a);
    final OpenCLComputer computer = new OpenCLComputer(true, true, c);
    computer.initialize();
    computer.compute();
    computer.release();
  }

  class TestPrefetchCopyComputation implements OpenCLComputation {

    final static int workgroupSize = 16;

    private final static String kernel_file_copy_prefetch_2d =
        "test/ca/uwaterloo/gpuclu/prefetch_copy_2d.cl";

    final PackedArrayImpl p;

    final Mem[] mems;
    final Dim[] dims;

    final MemPacked input;
    final MemInt unpacked_output;

    TestPrefetchCopyComputation(final PackedArrayImpl p) {
      this.p = p;
      this.input =
          new MemPacked("input", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> p.cellCount()), p.config.bitwidth, true, workgroupSize, 2);
      this.unpacked_output =
          new MemInt("unpacked_output", MemConfig.DownloadOnly, new MemCapacity(() -> p.capacity()));
      this.mems = new Mem[] {input, unpacked_output};
      this.dims =
          new Dim[] {new Dim(0, (int) Math.sqrt(input.logicalCapacity())),
              new Dim(1, (int) Math.sqrt(input.logicalCapacity()))};
    }

    @Override
    public Mem[] memories() {
      return mems;
    }

    @Override
    public Dim[] dimensions() {
      return dims;
    }

    @Override
    public Kernel[] kernels() {
      return kernels;
    }

    final Kernel[] kernels = new Kernel[] {new Kernel(kernel_file_copy_prefetch_2d,
        "prefetch_copy_2d") {

      @Override
      public cl_event prepareLaunchInner() {
        setArg("input", Sizeof.cl_mem, input.d_pointer());
        setArg("dim_length", Sizeof.cl_int, OpenCLUtil.pointerToInt(dims[0].size));
        setArg("output", Sizeof.cl_mem, unpacked_output.d_pointer());

        // launch prefetch_copy kernel
        final cl_event kernel_execution_event = new cl_event();
        final int work_items = dims[0].size * dims[1].size;
        total_work_items += work_items;

        work_dim = dims.length;
        global_work_offset = null;
        global_work_size = new long[] {dims[0].size, dims[1].size};
        System.out.println("wi is " + total_work_items + " sizes are " + dims[0].size);
        local_work_size =
            new long[] {(int) Math.sqrt(workgroupSize), (int) Math.sqrt(workgroupSize)}; // null;
        num_events_in_wait_list = 0;
        event_wait_list = null;
        event = kernel_execution_event;

        return kernel_execution_event;
      }
    }};


    @Override
    public void uploadPreprocessing() {
      final Random r = new Random();
      for (int i = 0; i < input.logicalCapacity(); i++) {
        final int val = r.nextInt(16);
        p.append(val);
        input.h_append(val);
      }
      try {
        handler = new FileHandler("prefetch_copy_log%u.txt");
      } catch (final IOException e) {
        System.out.println("Could not create file. Using the console handler");
      }
      logger.addHandler(handler);
    }

    @Override
    public void uploadPostprocessing() {}

    @Override
    public void downloadPreprocessing() {}

    @Override
    public void downloadPostprocessing() {
      for (int i = 0; i < input.logicalCapacity(); i++) {
        assertEquals(p.get(i), unpacked_output.h[i]);
      }
    }

    @Override
    public Logger logger() {
      return logger;
    }

    private final Logger logger = Logger.getLogger(TestPrefetchCopyComputation.class
        .getSimpleName());
    private Handler handler;

  }

}
