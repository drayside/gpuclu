__kernel void prefetch_sum(
	__global const uint* const g_input,
	const uint g_input_cell_count,
	__global uint* const output)
{
    const int workID = get_global_id(0);

	INIT_VAR_input(g_input,input)

	for(uint index = 0; index < g_input_cell_count; index++)
		output[workID] += input_get(input, index);
}
