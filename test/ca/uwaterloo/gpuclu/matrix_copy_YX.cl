__kernel void matrix_copy_YX(
	__global const uint* const linear_matrix, 
	const uint length,
	__global uint* const output)
{
    const int y = get_global_id(0);
    const int x = get_global_id(1);

	const int index = x*length + y;

    const int bit = linear_matrix[index];

	output[index] = bit;
}
