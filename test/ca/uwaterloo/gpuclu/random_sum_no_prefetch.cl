__kernel void random_sum_no_prefetch(
	__global const uint* const no_prefetch_input, 
	__global const uint* const no_prefetch_indices, 
	__global uint* const no_prefetch_sum)
{
    const int tid = get_global_id(0);

    const int index = no_prefetch_indices_get(no_prefetch_indices, tid);

	const int val = no_prefetch_input_get(no_prefetch_input, index);

    atomic_add(&no_prefetch_sum[0], val);   
}

