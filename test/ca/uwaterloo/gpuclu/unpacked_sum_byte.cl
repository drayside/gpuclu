__kernel void unpacked_sum_byte(
	__global const uchar* const unpacked_input, 
	__global uint* const sum)
{
    const int tid = get_global_id(0);

    const uchar bit = unpacked_input[tid];

	atomic_add(&sum[0], bit);
}
