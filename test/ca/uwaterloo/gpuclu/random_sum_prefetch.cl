__kernel void random_sum_prefetch(
	__global const uint* const g_prefetch_input,
	const uint g_prefetch_input_cell_count,
	__global const uint* const prefetch_indices, 
	__global uint* const prefetch_sum)
{
    const int workID = get_global_id(0);
    const int threadID = get_local_id(0);

	INIT_VAR_prefetch_input(0,threadID,g_prefetch_input,g_prefetch_input_cell_count,prefetch_input)
	
    const int index = prefetch_indices_get(prefetch_indices, workID);

	const int val = prefetch_input_get(prefetch_input, index);

	atomic_add(&prefetch_sum[0], val);
}
