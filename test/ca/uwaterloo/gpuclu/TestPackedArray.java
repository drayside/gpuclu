package ca.uwaterloo.gpuclu;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Random;
import java.util.logging.Logger;

import org.jocl.Sizeof;
import org.jocl.cl_event;
import org.junit.Test;

public class TestPackedArray {

  @Test
  public void testConstructor1() {
    final PackedArrayImpl a = new PackedArrayImpl(1, 1);
    assertEquals(1, a.config.bitwidth);
    assertEquals(32, a.config.indicesPerCell);
    assertEquals(1, a.config.valueMask);
    assertEquals(0, a.size());
    assertEquals(a.config.indicesPerCell, a.capacity());
  }

  @Test
  public void testConstructor2() {
    final PackedArrayImpl a = new PackedArrayImpl(2, 1);
    assertEquals(2, a.config.bitwidth);
    assertEquals(16, a.config.indicesPerCell);
    assertEquals(3, a.config.valueMask);
    assertEquals(0, a.size());
    assertEquals(a.config.indicesPerCell, a.capacity());
  }

  @Test
  public void testConstructor7() {
    final PackedArrayImpl a = new PackedArrayImpl(7, 1);
    assertEquals(8, a.config.bitwidth);
    assertEquals(4, a.config.indicesPerCell);
    assertEquals(255, a.config.valueMask);
    assertEquals(0, a.size());
    assertEquals(a.config.indicesPerCell, a.capacity());
  }

  @Test
  public void testConstructor8() {
    final PackedArrayImpl a = new PackedArrayImpl(8, 1);
    assertEquals(8, a.config.bitwidth);
    assertEquals(4, a.config.indicesPerCell);
    assertEquals(255, a.config.valueMask);
    assertEquals(0, a.size());
    assertEquals(a.config.indicesPerCell, a.capacity());
  }

  @Test
  public void testConstructor9() {
    final PackedArrayImpl a = new PackedArrayImpl(9, 1);
    assertEquals(16, a.config.bitwidth);
    assertEquals(2, a.config.indicesPerCell);
    assertEquals(65536 - 1, a.config.valueMask);
    assertEquals(0, a.size());
    assertEquals(a.config.indicesPerCell, a.capacity());
  }

  @Test
  public void testConstructor17() {
    final PackedArrayImpl a = new PackedArrayImpl(17, 1);
    assertEquals(32, a.config.bitwidth);
    assertEquals(1, a.config.indicesPerCell);
    assertEquals(0xFFFFFFFF, a.config.valueMask);
    // assertEquals(Integer.MAX_VALUE, a.mask);
    assertEquals(0, a.size());
    assertEquals(a.config.indicesPerCell, a.capacity());
  }

  @Test
  public void testConstructor31() {
    final PackedArrayImpl a = new PackedArrayImpl(31, 1);
    assertEquals(32, a.config.bitwidth);
    assertEquals(1, a.config.indicesPerCell);
    assertEquals(0xFFFFFFFF, a.config.valueMask);
    assertEquals(0, a.size());
    assertEquals(a.config.indicesPerCell, a.capacity());
  }

  @Test
  public void testConstructor32() {
    final PackedArrayImpl a = new PackedArrayImpl(32, 1);
    assertEquals(32, a.config.bitwidth);
    assertEquals(1, a.config.indicesPerCell);
    assertEquals(0xFFFFFFFF, a.config.valueMask);
    assertEquals(0, a.size());
    assertEquals(a.config.indicesPerCell, a.capacity());
  }

  @Test
  public void testSetGet0() {
    final PackedArrayImpl a = new PackedArrayImpl(8, 1);
    for (int i = 0; i < 256; i++) {
      a.set(0, i);
      assertEquals(i, a.get(0));
    }
  }

  @Test
  public void testSetGet1() {
    final PackedArrayImpl a = new PackedArrayImpl(8, 3);
    // everything should be zero to start
    assertEquals(0, a.get(0));
    assertEquals(0, a.get(1));
    assertEquals(0, a.get(2));
    // try setting 1 in the second position
    a.set(1, 1);
    assertEquals(1, a.get(1));
    assertEquals(0, a.get(0));
    assertEquals(0, a.get(2));

    // try setting some values at the second position (i.e., index 1)
    for (int i = 0; i < 255; i++) {
      a.set(1, i);
      assertEquals("i==" + i, 0, a.get(0));
      assertEquals("i==" + i, i, a.get(1));
      assertEquals("i==" + i, 0, a.get(2));
    }
  }

  @Test
  public void testCell() {
    final int initialSize = 5;
    final PackedArrayImpl a = new PackedArrayImpl(8, initialSize);
    assertEquals(0, a.cell(0));
    assertEquals(0, a.cell(1));
    assertEquals(0, a.cell(2));
    assertEquals(0, a.cell(3));
    assertEquals(1, a.cell(4));
    assertEquals(1, a.cell(5));
    assertEquals(1, a.cell(6));
    assertEquals(1, a.cell(7));
    assertEquals(2, a.cell(8));
    assertEquals(2, a.cell(9));
    assertEquals(2, a.cell(10));
    assertEquals(2, a.cell(11));
  }

  @Test
  public void testSubcell() {
    final int initialSize = 100;
    for (int bitwidth = 2; bitwidth < 32; bitwidth++) {
      testSubcell(initialSize, bitwidth);
    }
  }

  private void testSubcell(final int initialSize, final int bitwidth) {
    final PackedArrayImpl a = new PackedArrayImpl(bitwidth, initialSize);
    int index = 0;
    for (int cell = 0; cell < a.config.indicesPerCell; cell++) {
      for (int subcell = 0; subcell < 32; subcell += a.config.bitwidth) {
        // System.out.println(Util.i2s(index, cell, subcell) + " " +
        // Integer.toBinaryString(subcell));
        assertEquals("cell", cell, a.cell(index));
        assertEquals("subcell", subcell, a.subcell(index));
        index++;
      }
    }
  }

  @Test
  public void testSetGet2() {
    final int initialSize = 5;
    final PackedArrayImpl a = new PackedArrayImpl(8, initialSize);

    final int m = 5;
    for (int i = 0; i < initialSize; i++) {
      a.set(i, i % m);
    }
    for (int i = 0; i < initialSize; i++) {
      assertEquals("i==" + i, i % m, a.get(i));
    }
  }

  @Test
  public void testSetGet3() {
    final int initialSize = 10;
    final PackedArrayImpl a = new PackedArrayImpl(2, initialSize);
    final int[] b = new int[initialSize];
    for (int i = 0; i < initialSize; i++) {
      final int v = Math.random() > 0.5 ? 0 : 1;
      a.set(i, v);
      b[i] = v;
    }
    for (int i = 0; i < initialSize; i++) {
      assertEquals("i: " + i, b[i], a.get(i));
    }
  }

  @Test
  public void testRandom() {
    final Random r = new Random();
    for (int i = 0; i < 1000; i++) {
      final int bitwidth = r.nextInt(30) + 1;
      final int size = r.nextInt(1000);
      testRandom(r, size, bitwidth);
    }
  }

  private void testRandom(final Random r, final int initialSize, final int bitwidth) {
    final PackedArrayImpl a = new PackedArrayImpl(bitwidth, initialSize);
    final int[] b = new int[initialSize];
    for (int i = 0; i < initialSize; i++) {
      final int v = r.nextInt(a.maxValue());
      a.set(i, v);
      b[i] = v;
    }
    for (int i = 0; i < initialSize; i++) {
      assertEquals("bitwidth, size, i: " + Util.i2s(bitwidth, initialSize, i), b[i], a.get(i));
    }
  }

  @Test
  public void testLeftovers() {
    final PackedArrayImpl a = new PackedArrayImpl(8, 7);
    assertFalse(a.hasLeftovers());
    final PackedArrayImpl a2 = a.getLeftovers();
    assertEquals(0, a2.size());


    a.set(0, 1);
    assertTrue(a.hasLeftovers());
    final PackedArrayImpl b = a.getLeftovers();
    assertEquals(1, b.size());
    assertEquals(1, b.get(0));

    a.set(3, 4);
    assertFalse(a.hasLeftovers());

    a.set(6, 7);
    assertTrue(a.hasLeftovers());
    final PackedArrayImpl c = a.getLeftovers();
    assertEquals(3, c.size());
    assertEquals(0, c.get(0));
    assertEquals(0, c.get(1));
    assertEquals(7, c.get(2));

  }

  @Test
  public void testSubArray() {
    final int sectionCount = 103;
    final int indicesPerSection = 17;
    final PackedArrayImpl a = new PackedArrayImpl("test", 2, sectionCount, indicesPerSection);
    // set everything, including padding bits
    final int[] b = new int[a.capacity()];
    for (int i = 0; i < a.capacity(); i++) {
      final int v = i % 3;
      a.set(i, v);
      b[i] = v;
    }
    for (int i = 0; i < sectionCount; i++) {
      final PackedArray s = a.section(i);
      final int baseIndex = i * s.capacity();
      // read back just the data bits (i.e., not the padding bits)
      for (int j = 0; j < s.size(); j++) {
        final int expected = b[baseIndex + j];
        assertEquals("section, baseIndex, j: " + Util.i2s(i, baseIndex, j), expected, s.get(j));
        s.set(j, 0);
        assertEquals(0, s.get(j));
      }
    }
  }

  @Test
  public void testRecalibrate() {
    final int initialSize = 5;
    final PackedArrayImpl a = new PackedArrayImpl(8, initialSize);
    final PackedArrayImpl b = new PackedArrayImpl(8, initialSize);
    a.set(3, 5);
    b.set(2, 6);
    assert a.size() == 4;
    assert b.size() == 3;
    final int[] aCells = a.getCells();
    final int[] bCells = b.getCells();
    for (int i = 0; i < aCells.length; i++) {
      bCells[i] = aCells[i];
    }
    b.recalibrateSize();
    assert b.size() == 4;
  }

  @Test
  public void testGPU() {
    final PackedArrayImpl a = new PackedArrayImpl(2, 1);
    final OpenCLComputation c = new PackedArrayTestComputation(a);
    final OpenCLComputer computer = new OpenCLComputer(true, true, c);
    computer.initialize();
    ((PackedArrayTestComputation) c).inputMem().h_append(2);
    computer.compute();
    computer.release();
  }

  @Test
  public void testGPU2() {
    final PackedArrayImpl a = new PackedArrayImpl(2, 10);
    final PackedArrayTestComputation c = new PackedArrayTestComputation(a);
    final OpenCLComputer computer = new OpenCLComputer(true, true, c);
    computer.initialize();
    c.inputMem().h_append(2);
    c.inputMem().h_append(3);
    c.inputMem().h_append(1);
    c.inputMem().h_append(1);
    computer.compute();
    c.inputMem().h_append(2);
    c.inputMem().h_append(3);
    computer.compute();
    computer.release();
  }


  @Test
  public void testPackedSumGPU() {
    final PackedArrayImpl a = new PackedArrayImpl(2, 1024 * 1024);
    final OpenCLComputation c = new ArraySumPackedTestComputation(a);
    final OpenCLComputer computer = new OpenCLComputer(true, true, c);
    computer.initialize();
    computer.compute();
    computer.release();
  }

  @Test
  public void testUnpackedSumGPU() {
    final OpenCLComputation c = new ArraySumUnpackedTestComputation(1024 * 1024);
    final OpenCLComputer computer = new OpenCLComputer(true, true, c);
    computer.initialize();
    computer.compute();
    computer.release();
  }

  @Test
  public void testPackedSumByteGPU() {
    final PackedArrayImpl a = new PackedArrayImpl(8, 1024 * 1024);
    final OpenCLComputation c = new ArrayByteSumPackedTestComputation(a);
    final OpenCLComputer computer = new OpenCLComputer(true, true, c);
    computer.initialize();
    computer.compute();
    computer.release();
  }

  @Test
  public void testUnpackedSumByteGPU() {
    final OpenCLComputation c = new ArrayByteSumUnpackedTestComputation(1024 * 1024);
    final OpenCLComputer computer = new OpenCLComputer(true, true, c);
    computer.initialize();
    computer.compute();
    computer.release();
  }

  @Test
  public void testRandomSumGPU() {
    final PackedArrayImpl a = new PackedArrayImpl(32, 5);
    final PackedArrayImpl b = new PackedArrayImpl(32, 5000);
    final OpenCLComputation c = new RandomSumTestComputation(a, b);
    final OpenCLComputer computer = new OpenCLComputer(true, true, c);
    computer.initialize();
    computer.compute();
    computer.release();
  }

  class PackedArrayTestComputation implements OpenCLComputation {

    final static boolean prefetch = false;
    final static int workgroupSize = 32;

    private final static String kernel_file_copy_int = "src/ca/uwaterloo/gpuclu/copy_int.cl";
    private final static String kernel_file_test_packed = "test/ca/uwaterloo/gpuclu/test_packed.cl";

    final PackedArrayImpl p;

    final Mem[] mems;
    final Dim[] dims;

    final MemPacked input;
    final MemPacked packed_output1;
    final MemPacked packed_output2;
    final MemInt unpacked_output;

    PackedArrayTestComputation(final PackedArrayImpl p) {
      this.p = p;
      this.input =
          new MemPacked("input", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> p.cellCount()), p.config.bitwidth, prefetch, workgroupSize, p.cellCount());
      this.packed_output1 =
          new MemPacked("packed_output1", MemConfig.DownloadOnly, new MemCapacity(
              () -> p.cellCount()), p.config.bitwidth, prefetch, workgroupSize, p.cellCount());
      this.packed_output2 =
          new MemPacked("packed_output2", MemConfig.DownloadOnly, new MemCapacity(
              () -> p.cellCount()), p.config.bitwidth, prefetch, workgroupSize, p.cellCount());
      this.unpacked_output =
          new MemInt("unpacked_output", MemConfig.DownloadOnly, new MemCapacity(() -> p.capacity()));
      this.mems = new Mem[] {input, packed_output1, packed_output2, unpacked_output};
      this.dims = new Dim[] {new Dim(0, input.capacity.deviceCapacity())};
    }

    public MemPacked inputMem() {
      return input;
    }

    @Override
    public Mem[] memories() {
      return mems;
    }

    @Override
    public Dim[] dimensions() {
      return dims;
    }

    @Override
    public Kernel[] kernels() {
      return kernels;
    }

    final Kernel[] kernels = new Kernel[] {new Kernel(kernel_file_copy_int, "copy_int") {

      @Override
      public cl_event prepareLaunchInner() {
        setArg("source", Sizeof.cl_mem, input.d_pointer());
        setArg("target", Sizeof.cl_mem, packed_output1.d_pointer());
        setArg("length", Sizeof.cl_int, OpenCLUtil.pointerToInt(input.capacity.deviceCapacity()));

        // launch copy_int kernel
        final cl_event kernel_execution_event = new cl_event();
        final int work_items = dims[0].size;// input.capacity.deviceCapacity();
        total_work_items += work_items;

        work_dim = dims.length;
        global_work_offset = null;
        global_work_size = new long[] {work_items};
        local_work_size = null;
        num_events_in_wait_list = 0;
        event_wait_list = null;
        event = kernel_execution_event;

        return kernel_execution_event;
      }
    }, new Kernel(kernel_file_test_packed, "test_packed") {

      @Override
      public cl_event prepareLaunchInner() {
        setArg(input);
        setArg(packed_output2);
        setArg(unpacked_output);

        // launch test_packed kernel
        final cl_event kernel_execution_event = new cl_event();
        final int work_items = input.logicalCapacity();
        total_work_items += work_items;

        work_dim = dims.length;
        global_work_offset = null;
        global_work_size = new long[] {work_items};
        local_work_size = null;
        num_events_in_wait_list = 0;
        event_wait_list = null;
        event = kernel_execution_event;

        return kernel_execution_event;
      }
    }};


    @Override
    public void uploadPreprocessing() {}

    @Override
    public void uploadPostprocessing() {}

    @Override
    public void downloadPreprocessing() {}

    @Override
    public void downloadPostprocessing() {
      assert input.h.capacity() == packed_output1.h.capacity() : "input/output capacities don't match: "
          + Util.i2s(input.h.capacity(), packed_output1.h.capacity());
      assert input.h.capacity() == packed_output2.h.capacity() : "input/output capacities don't match: "
          + Util.i2s(input.h.capacity(), packed_output2.h.capacity());


      assert input.h.size() == packed_output1.h.size() : "input/output sizes don't match: "
          + Util.i2s(input.h.size(), packed_output1.h.size());
      assert input.h.size() == packed_output2.h.size() : "input/output sizes don't match: "
          + Util.i2s(input.h.size(), packed_output2.h.size());
      assert input.h.capacity() == unpacked_output.h_length() : "input/output capacity/size doesn't match: "
          + Util.i2s(input.h.capacity(), unpacked_output.h_length());

      for (int i = 0; i < unpacked_output.h_length(); i++) {
        final int x = unpacked_output.h[i];
        final int y = input.h.get(i);
        final int z = packed_output1.h.get(i);
        final int z2 = packed_output2.h.get(i);
        assert x == y : "values differ at index " + i;
        assert x == z : "values differ at index " + i;
        assert z == z2 : "values differ at index " + i;
      }
    }

    @Override
    public Logger logger() {
      return logger;
    }

    private final Logger logger = Logger.getLogger(TestPackedArray.class.getSimpleName());

  }

  class ArraySumPackedTestComputation implements OpenCLComputation {

    final static boolean prefetch = false;
    final static int workgroupSize = 32;

    private final static String kernel_file_packed_sum = "test/ca/uwaterloo/gpuclu/packed_sum.cl";

    final PackedArrayImpl p;

    final Mem[] mems;
    final Dim[] dims;

    final MemPacked packed_input;
    final MemInt packed_output;

    ArraySumPackedTestComputation(final PackedArrayImpl p) {
      this.p = p;
      this.packed_input =
          new MemPacked("packed_input", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> p.cellCount()), p.config.bitwidth, prefetch, workgroupSize, p.cellCount());
      this.packed_output =
          new MemInt("packed_output", MemConfig.DownloadOnly, new MemCapacity(() -> 1));
      this.mems = new Mem[] {packed_input, packed_output};
      this.dims = new Dim[] {new Dim(0, packed_input.logicalCapacity())};
    }

    @Override
    public Mem[] memories() {
      return mems;
    }

    @Override
    public Dim[] dimensions() {
      return dims;
    }

    @Override
    public Kernel[] kernels() {
      return kernels;
    }

    final Kernel[] kernels = new Kernel[] {new Kernel(kernel_file_packed_sum, "packed_sum") {

      @Override
      public cl_event prepareLaunchInner() {
        setArg("packed_input", Sizeof.cl_mem, packed_input.d_pointer());
        setArg("sum", Sizeof.cl_mem, packed_output.d_pointer());

        // launch packed_sum kernel
        final cl_event kernel_execution_event = new cl_event();
        final int work_items = dims[0].size; // packed_input.logicalCapacity();
        total_work_items += work_items;

        work_dim = dims.length;
        global_work_offset = null;
        global_work_size = new long[] {work_items};
        local_work_size = null;
        num_events_in_wait_list = 0;
        event_wait_list = null;
        event = kernel_execution_event;

        return kernel_execution_event;
      }
    }};


    @Override
    public void uploadPreprocessing() {
      packed_output.h[0] = 0;
      for (int i = 0; i < packed_input.logicalCapacity(); i++) {
        packed_input.h_append(1);
      }
    }

    @Override
    public void uploadPostprocessing() {}

    @Override
    public void downloadPreprocessing() {}

    @Override
    public void downloadPostprocessing() {
      assert packed_output.h[0] == dims[0].size;
    }

    @Override
    public Logger logger() {
      return logger;
    }

    private final Logger logger = Logger.getLogger(TestPackedArray.class.getSimpleName());

  }

  class ArraySumUnpackedTestComputation implements OpenCLComputation {

    final static boolean prefetch = false;
    final static int workgroupSize = 32;

    private final static String kernel_file_unpacked_sum =
        "test/ca/uwaterloo/gpuclu/unpacked_sum.cl";

    int length;

    final Mem[] mems;
    final Dim[] dims;

    final MemInt unpacked_input;
    final MemInt unpacked_output;

    ArraySumUnpackedTestComputation(final int l) {
      this.length = l;
      this.unpacked_input =
          new MemInt("unpacked_input", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> length));
      this.unpacked_output =
          new MemInt("unpacked_output", MemConfig.DownloadOnly, new MemCapacity(() -> 1));
      this.mems = new Mem[] {unpacked_input, unpacked_output};
      this.dims = new Dim[] {new Dim(0, length)};
    }

    @Override
    public Mem[] memories() {
      return mems;
    }

    @Override
    public Dim[] dimensions() {
      return dims;
    }

    @Override
    public Kernel[] kernels() {
      return kernels;
    }

    final Kernel[] kernels = new Kernel[] {new Kernel(kernel_file_unpacked_sum, "unpacked_sum") {

      @Override
      public cl_event prepareLaunchInner() {
        setArg("unpacked_input", Sizeof.cl_mem, unpacked_input.d_pointer());
        setArg("sum", Sizeof.cl_mem, unpacked_output.d_pointer());

        // launch packed_sum kernel
        final cl_event kernel_execution_event = new cl_event();
        final int work_items = dims[0].size;
        total_work_items += work_items;

        work_dim = dims.length;
        global_work_offset = null;
        global_work_size = new long[] {work_items};
        local_work_size = null;
        num_events_in_wait_list = 0;
        event_wait_list = null;
        event = kernel_execution_event;

        return kernel_execution_event;
      }
    }};


    @Override
    public void uploadPreprocessing() {
      unpacked_output.h[0] = 0;
      for (int i = 0; i < dims[0].size; i++) {
        unpacked_input.h[i] = 1;
      }
    }

    @Override
    public void uploadPostprocessing() {}

    @Override
    public void downloadPreprocessing() {}

    @Override
    public void downloadPostprocessing() {
      assert unpacked_output.h[0] == dims[0].size;
    }

    @Override
    public Logger logger() {
      return logger;
    }

    private final Logger logger = Logger.getLogger(TestPackedArray.class.getSimpleName());

  }

  class ArrayByteSumPackedTestComputation implements OpenCLComputation {

    final static boolean prefetch = false;
    final static int workgroupSize = 32;

    private final static String kernel_file_packed_sum = "test/ca/uwaterloo/gpuclu/packed_sum.cl";

    final PackedArrayImpl p;

    final Mem[] mems;
    final Dim[] dims;

    final MemPacked packed_input;
    final MemInt packed_output;

    ArrayByteSumPackedTestComputation(final PackedArrayImpl p) {
      this.p = p;
      this.packed_input =
          new MemPacked("packed_input", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> p.cellCount()), p.config.bitwidth, prefetch, workgroupSize, p.cellCount());
      this.packed_output =
          new MemInt("packed_output", MemConfig.DownloadOnly, new MemCapacity(() -> 1));
      this.mems = new Mem[] {packed_input, packed_output};
      this.dims = new Dim[] {new Dim(0, packed_input.logicalCapacity())};
    }

    @Override
    public Mem[] memories() {
      return mems;
    }

    @Override
    public Dim[] dimensions() {
      return dims;
    }

    @Override
    public Kernel[] kernels() {
      return kernels;
    }

    final Kernel[] kernels = new Kernel[] {new Kernel(kernel_file_packed_sum, "packed_sum") {

      @Override
      public cl_event prepareLaunchInner() {
        setArg("packed_input", Sizeof.cl_mem, packed_input.d_pointer());
        setArg("sum", Sizeof.cl_mem, packed_output.d_pointer());

        // launch packed_sum kernel
        final cl_event kernel_execution_event = new cl_event();
        final int work_items = dims[0].size; // packed_input.logicalCapacity();
        total_work_items += work_items;

        work_dim = dims.length;
        global_work_offset = null;
        global_work_size = new long[] {work_items};
        local_work_size = null;
        num_events_in_wait_list = 0;
        event_wait_list = null;
        event = kernel_execution_event;

        return kernel_execution_event;
      }
    }};


    @Override
    public void uploadPreprocessing() {
      packed_output.h[0] = 0;
      for (int i = 0; i < packed_input.logicalCapacity(); i++) {
        packed_input.h_append(100);
      }
    }

    @Override
    public void uploadPostprocessing() {}

    @Override
    public void downloadPreprocessing() {}

    @Override
    public void downloadPostprocessing() {
      assert packed_output.h[0] == dims[0].size * 100;
    }

    @Override
    public Logger logger() {
      return logger;
    }

    private final Logger logger = Logger.getLogger(TestPackedArray.class.getSimpleName());

  }

  class ArrayByteSumUnpackedTestComputation implements OpenCLComputation {

    final static boolean prefetch = false;
    final static int workgroupSize = 32;

    private final static String kernel_file_unpacked_sum_byte =
        "test/ca/uwaterloo/gpuclu/unpacked_sum_byte.cl";

    int length;

    final Mem[] mems;
    final Dim[] dims;

    final MemByte unpacked_input;
    final MemInt unpacked_output;

    ArrayByteSumUnpackedTestComputation(final int l) {
      this.length = l;
      this.unpacked_input =
          new MemByte("unpacked_input", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> length));
      this.unpacked_output =
          new MemInt("unpacked_output", MemConfig.DownloadOnly, new MemCapacity(() -> 1));
      this.mems = new Mem[] {unpacked_input, unpacked_output};
      this.dims = new Dim[] {new Dim(0, length)};
    }

    @Override
    public Mem[] memories() {
      return mems;
    }

    @Override
    public Dim[] dimensions() {
      return dims;
    }

    @Override
    public Kernel[] kernels() {
      return kernels;
    }

    final Kernel[] kernels = new Kernel[] {new Kernel(kernel_file_unpacked_sum_byte,
        "unpacked_sum_byte") {

      @Override
      public cl_event prepareLaunchInner() {
        setArg("unpacked_input", Sizeof.cl_mem, unpacked_input.d_pointer());
        setArg("sum", Sizeof.cl_mem, unpacked_output.d_pointer());

        // launch packed_sum kernel
        final cl_event kernel_execution_event = new cl_event();
        final int work_items = dims[0].size;
        total_work_items += work_items;

        work_dim = dims.length;
        global_work_offset = null;
        global_work_size = new long[] {work_items};
        local_work_size = null;
        num_events_in_wait_list = 0;
        event_wait_list = null;
        event = kernel_execution_event;

        return kernel_execution_event;
      }
    }};


    @Override
    public void uploadPreprocessing() {
      unpacked_output.h[0] = 0;
      for (int i = 0; i < dims[0].size; i++) {
        unpacked_input.h[i] = 100;
      }
    }

    @Override
    public void uploadPostprocessing() {}

    @Override
    public void downloadPreprocessing() {}

    @Override
    public void downloadPostprocessing() {
      assert unpacked_output.h[0] == dims[0].size * 100;
    }

    @Override
    public Logger logger() {
      return logger;
    }

    private final Logger logger = Logger.getLogger(TestPackedArray.class.getSimpleName());

  }

  class RandomSumTestComputation implements OpenCLComputation {

    final static int workgroupSize = 32;

    private final static String kernel_file_random_sum_prefetch =
        "test/ca/uwaterloo/gpuclu/random_sum_prefetch.cl";
    private final static String kernel_file_random_sum_no_prefetch =
        "test/ca/uwaterloo/gpuclu/random_sum_no_prefetch.cl";

    final PackedArrayImpl p, q;

    final Mem[] mems;
    final Dim[] dims;

    final MemPacked prefetch_input;
    final MemPacked no_prefetch_input;
    final MemPacked prefetch_indices;
    final MemPacked no_prefetch_indices;
    final MemInt prefetch_output;
    final MemInt no_prefetch_output;

    RandomSumTestComputation(final PackedArrayImpl p, final PackedArrayImpl q) {
      this.p = p;
      this.q = q;
      final boolean flag = true;
      this.prefetch_input =
          new MemPacked("prefetch_input", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> p.cellCount()), p.config.bitwidth, flag, workgroupSize, p.cellCount());
      this.no_prefetch_input =
          new MemPacked("no_prefetch_input", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> p.cellCount()), p.config.bitwidth, false, workgroupSize, p.cellCount());
      this.prefetch_indices =
          new MemPacked("prefetch_indices", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> q.cellCount()), q.config.bitwidth, false, workgroupSize, q.cellCount());
      this.no_prefetch_indices =
          new MemPacked("no_prefetch_indices", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> q.cellCount()), q.config.bitwidth, false, workgroupSize, q.cellCount());
      this.prefetch_output =
          new MemInt("prefetch_sum", MemConfig.DownloadOnly, new MemCapacity(() -> 1));
      this.no_prefetch_output =
          new MemInt("no_prefetch_sum", MemConfig.DownloadOnly, new MemCapacity(() -> 1));
      this.mems =
          new Mem[] {prefetch_input, no_prefetch_input, prefetch_indices, no_prefetch_indices,
              prefetch_output, no_prefetch_output};
      this.dims = new Dim[] {new Dim(0, prefetch_indices.logicalCapacity())};
    }

    @Override
    public Mem[] memories() {
      return mems;
    }

    @Override
    public Dim[] dimensions() {
      return dims;
    }

    @Override
    public Kernel[] kernels() {
      return kernels;
    }

    final Kernel[] kernels = new Kernel[] {
        new Kernel(kernel_file_random_sum_prefetch, "random_sum_prefetch") {

          @Override
          public cl_event prepareLaunchInner() {
            setArg("prefetch_input", Sizeof.cl_mem, prefetch_input.d_pointer());
            setArg("prefetch_input_cell_count", Sizeof.cl_int,
                OpenCLUtil.pointerToInt(prefetch_input.h_length()));
            setArg("prefetch_indices", Sizeof.cl_mem, prefetch_indices.d_pointer());
            setArg("prefetch_sum", Sizeof.cl_mem, prefetch_output.d_pointer());

            // launch packed_sum kernel
            final cl_event kernel_execution_event = new cl_event();
            final int work_items = dims[0].size; // prefetch_indices.logicalCapacity();
            total_work_items += work_items;

            work_dim = dims.length;
            global_work_offset = null;
            global_work_size = new long[] {work_items};
            local_work_size = null;
            num_events_in_wait_list = 0;
            event_wait_list = null;
            event = kernel_execution_event;

            return kernel_execution_event;
          }
        }, new Kernel(kernel_file_random_sum_no_prefetch, "random_sum_no_prefetch") {

          @Override
          public cl_event prepareLaunchInner() {
            setArg("no_prefetch_input", Sizeof.cl_mem, no_prefetch_input.d_pointer());
            setArg("no_prefetch_indices", Sizeof.cl_mem, no_prefetch_indices.d_pointer());
            setArg("no_prefetch_sum", Sizeof.cl_mem, no_prefetch_output.d_pointer());

            // launch packed_sum kernel
            final cl_event kernel_execution_event = new cl_event();
            final int work_items = prefetch_indices.logicalCapacity();
            total_work_items += work_items;

            work_dim = dims.length;
            global_work_offset = null;
            global_work_size = new long[] {work_items};
            local_work_size = null;
            num_events_in_wait_list = 0;
            event_wait_list = null;
            event = kernel_execution_event;

            return kernel_execution_event;
          }
        }};


    @Override
    public void uploadPreprocessing() {
      prefetch_output.h[0] = 0;
      no_prefetch_output.h[0] = 0;
      // final Random r = new Random();
      for (int i = 0; i < prefetch_input.logicalCapacity(); i++) {
        prefetch_input.h_append(i);
        no_prefetch_input.h_append(i);
      }
      for (int i = 0; i < prefetch_indices.logicalCapacity(); i++) {
        // int x = r.nextInt(prefetch_input.logicalCapacity());
        prefetch_indices.h_append(1);
        no_prefetch_indices.h_append(1);
      }
    }

    @Override
    public void uploadPostprocessing() {}

    @Override
    public void downloadPreprocessing() {}

    @Override
    public void downloadPostprocessing() {
      assert prefetch_output.h[0] == no_prefetch_output.h[0] : Util.i2s(prefetch_output.h[0],
          no_prefetch_output.h[0]);
    }

    @Override
    public Logger logger() {
      return logger;
    }

    private final Logger logger = Logger.getLogger(TestPackedArray.class.getSimpleName());

  }

}
