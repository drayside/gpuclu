__kernel void test_packed(
	__global const uint* const input, 
	__global uint* const packed_output2,
	__global uint* const unpacked)
{
    const int tid = get_global_id(0);

	const int bit = input_get(input, tid);

    packed_output2_set(packed_output2, tid, bit);
    
    unpacked[tid] = bit;
}