__kernel void no_prefetch_sum(
	__global const uint* const input, 
	const uint g_input_cell_count,
	__global uint* const output)
{
    const int tid = get_global_id(0);

	for(uint index = 0; index < g_input_cell_count; index++)
		output[tid] += input_get(input, index);
}

