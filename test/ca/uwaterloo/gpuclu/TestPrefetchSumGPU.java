package ca.uwaterloo.gpuclu;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;

import org.jocl.Sizeof;
import org.jocl.cl_event;
import org.junit.Test;

public class TestPrefetchSumGPU {

  private static final int SIZE = 1024 * 1024 * 64;

  @Test
  public void testPrefetchSumGPU() {
    final PackedArrayImpl a = new PackedArrayImpl(32, 32);
    final OpenCLComputation c = new TestPrefetchSumComputation(a, SIZE);
    final OpenCLComputer computer = new OpenCLComputer(true, true, c);
    computer.initialize();
    computer.compute();
    computer.release();
  }

  @Test
  public void testNoPrefetchSumGPU() {
    final PackedArrayImpl a = new PackedArrayImpl(32, 32);
    final OpenCLComputation c = new TestNoPrefetchSumComputation(a, SIZE);
    final OpenCLComputer computer = new OpenCLComputer(true, true, c);
    computer.initialize();
    computer.compute();
    computer.release();
  }

  class TestPrefetchSumComputation implements OpenCLComputation {

    final static int workgroupSize = 32;

    private final static String kernel_file_sum_prefetch =
        "test/ca/uwaterloo/gpuclu/prefetch_sum.cl";

    final PackedArrayImpl p;

    final Mem[] mems;
    final Dim[] dims;

    final MemPacked input;
    final MemInt output;

    final int size;

    TestPrefetchSumComputation(final PackedArrayImpl p, final int s) {
      this.p = p;
      this.size = s;
      this.input =
          new MemPacked("input", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> p.cellCount()), p.config.bitwidth, true, workgroupSize, p.cellCount());

      this.output = new MemInt("output", MemConfig.DownloadOnly, new MemCapacity(() -> size));
      this.mems = new Mem[] {input, output};
      this.dims = new Dim[] {new Dim(0, size)};
    }

    @Override
    public Mem[] memories() {
      return mems;
    }

    @Override
    public Dim[] dimensions() {
      return dims;
    }

    @Override
    public Kernel[] kernels() {
      return kernels;
    }

    final Kernel[] kernels = new Kernel[] {new Kernel(kernel_file_sum_prefetch, "prefetch_sum") {

      @Override
      public cl_event prepareLaunchInner() {
        setArg("input", Sizeof.cl_mem, input.d_pointer());
        setArg("input_cell_count", Sizeof.cl_int, OpenCLUtil.pointerToInt(input.h_length()));
        setArg("output", Sizeof.cl_mem, output.d_pointer());

        // launch prefetch_sum kernel
        final cl_event kernel_execution_event = new cl_event();
        final int work_items = size;
        total_work_items += work_items;

        work_dim = dims.length;
        global_work_offset = null;
        global_work_size = new long[] {work_items};
        local_work_size = new long[] {workgroupSize}; // null;
        num_events_in_wait_list = 0;
        event_wait_list = null;
        event = kernel_execution_event;

        return kernel_execution_event;
      }
    }};


    @Override
    public void uploadPreprocessing() {
      for (int i = 0; i < input.logicalCapacity(); i++) {
        input.h_append(i);
      }
      try {
        handler = new FileHandler("prefetch_sum_log%u.txt");
      } catch (final IOException e) {
        System.out.println("Could not create file. Using the console handler");
      }
      logger.addHandler(handler);
    }

    @Override
    public void uploadPostprocessing() {}

    @Override
    public void downloadPreprocessing() {}

    @Override
    public void downloadPostprocessing() {}

    @Override
    public Logger logger() {
      return logger;
    }

    private final Logger logger = Logger
        .getLogger(TestPrefetchSumComputation.class.getSimpleName());
    private Handler handler;

  }

  class TestNoPrefetchSumComputation implements OpenCLComputation {

    final static int workgroupSize = 32;

    private final static String kernel_file_sum_no_prefetch =
        "test/ca/uwaterloo/gpuclu/no_prefetch_sum.cl";

    final PackedArrayImpl p;

    final Mem[] mems;
    final Dim[] dims;

    final MemPacked input;
    final MemInt output;

    final int size;

    TestNoPrefetchSumComputation(final PackedArrayImpl p, final int s) {
      this.p = p;
      this.size = s;
      this.input =
          new MemPacked("input", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> p.cellCount()), p.config.bitwidth, false, workgroupSize, p.cellCount());
      this.output = new MemInt("output", MemConfig.DownloadOnly, new MemCapacity(() -> size));
      this.mems = new Mem[] {input, output};
      this.dims = new Dim[] {new Dim(0, size)};
    }

    @Override
    public Mem[] memories() {
      return mems;
    }

    @Override
    public Dim[] dimensions() {
      return dims;
    }

    @Override
    public Kernel[] kernels() {
      return kernels;
    }

    final Kernel[] kernels = new Kernel[] {new Kernel(kernel_file_sum_no_prefetch,
        "no_prefetch_sum") {

      @Override
      public cl_event prepareLaunchInner() {
        setArg("input", Sizeof.cl_mem, input.d_pointer());
        setArg("input_cell_count", Sizeof.cl_int, OpenCLUtil.pointerToInt(input.h_length()));
        setArg("output", Sizeof.cl_mem, output.d_pointer());

        // launch no_prefetch_sum kernel
        final cl_event kernel_execution_event = new cl_event();
        final int work_items = size;
        total_work_items += work_items;

        work_dim = dims.length;
        global_work_offset = null;
        global_work_size = new long[] {work_items};
        local_work_size = null;
        num_events_in_wait_list = 0;
        event_wait_list = null;
        event = kernel_execution_event;

        return kernel_execution_event;
      }
    }};


    @Override
    public void uploadPreprocessing() {
      for (int i = 0; i < input.logicalCapacity(); i++) {
        input.h_append(i);
      }
      try {
        handler = new FileHandler("no_prefetch_sum_log%u.txt");
      } catch (final IOException e) {
        System.out.println("Could not create file. Using the console handler");
      }
      logger.addHandler(handler);
    }

    @Override
    public void uploadPostprocessing() {}

    @Override
    public void downloadPreprocessing() {}

    @Override
    public void downloadPostprocessing() {}

    @Override
    public Logger logger() {
      return logger;
    }

    private final Logger logger = Logger.getLogger(TestNoPrefetchSumComputation.class
        .getSimpleName());
    private Handler handler;
  }

}
