package ca.uwaterloo.gpuclu;

import java.io.PrintWriter;
import java.net.URL;

import org.eclipse.imp.pdb.facts.ISourceLocation;
import org.junit.Test;
import org.rascalmpl.interpreter.Evaluator;
import org.rascalmpl.interpreter.IRascalMonitor;
import org.rascalmpl.interpreter.JavaToRascal;
import org.rascalmpl.interpreter.NullRascalMonitor;
import org.rascalmpl.uri.URIUtil;

public class TestRascalIntegration {

  @Test
  public void test() throws Exception {
    final String[] moduleNames =
        new String[] {"OpenCLTransform", "IO", "String", "List", "ParseTree", "OpenCLGrammar",
            "OpenCLTransform"};

    // Eclipse starts JUnit in the project directory
    final String projectDir = new java.io.File(".").getCanonicalPath();
    final String projectRascal = projectDir + "/rascal.src";

    final JavaToRascal j2r =
        new JavaToRascal(new PrintWriter(System.out), new PrintWriter(System.err));

    // this is in the JavaToRascal.main() method, but does not work here ...
    // j2r.voidValue("import List;");

    final Evaluator evaluator = j2r.getEvaluator();

    // reading from the jar in this way does not work
    // Could not import module ParseTree: can not find in search path
    // final String jar = "/home/drayside/bin/eclipse-luna/plugins/rascal_0.7.3.201506091427.jar";
    // evaluator.addRascalSearchPath(URIUtil.createFileLocation(jar));


    // Rascal plugin jar unzipped in this tmp directory
    final String dir = "/tmp/rascal_unzip";
    evaluator.addRascalSearchPath(URIUtil.createFileLocation(dir + "/org/rascalmpl/library"));
    final ISourceLocation probe1 = evaluator.getRascalResolver().resolveModule("ParseTree");
    if (probe1 == null) {
      System.err.println("Please unzip the Rascal jar to " + dir);
      final URL pt_url = ClassLoader.getSystemResource("org/rascalmpl/library/ParseTree.rsc");
      final String s1 = pt_url.getPath();
      final String jar = s1.substring(0, s1.indexOf('!'));
      System.err.println(jar);
      System.err.println("exiting ...");
      assert false;
      return;
    } else {
      System.out.println("found ParseTree.rsc in \t\t" + probe1);
    }

    evaluator.addRascalSearchPath(URIUtil.createFileLocation(projectRascal));
    final ISourceLocation probe2 = evaluator.getRascalResolver().resolveModule("OpenCLGrammar");
    if (probe2 == null) {
      System.err.println("Could not find OpenCLGrammar.rsc");
      assert false;
      return;
    } else {
      System.out.println("found OpenCLGrammar.rsc in \t" + probe2);
    }



    final IRascalMonitor monitor = new NullRascalMonitor();
    for (final String m : moduleNames) {
      evaluator.doImport(monitor, m);
    }

    final ISourceLocation kernel = URIUtil.createFileLocation(projectDir + "/test/prefetch.c");
    System.out.println("kernel to transform: \t\t" + kernel);
    final Object result = j2r.eval("main([\"input\"], " + kernel.toString() + ", 1)");
    System.out.println(result);

    System.out.println("done.");
  }

}
