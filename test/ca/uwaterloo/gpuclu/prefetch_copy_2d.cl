__kernel void prefetch_copy_2d(
	__global const uint* const g_input,
	const uint dim_length,
	__global uint* const output)
{
	INIT_VAR_2D_input(g_input, input)
	output[LINEAR_GLOBAL_ID_input] = input_get(input, LINEAR_GLOBAL_ID_input);
}
