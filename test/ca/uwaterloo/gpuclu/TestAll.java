package ca.uwaterloo.gpuclu;

import org.junit.Test;

public class TestAll {

  private static final int ITERATIONS = 3;

  TestPrefetchCopyGPU prefetch = new TestPrefetchCopyGPU();
  TestSumGPU sum = new TestSumGPU();
  TestSumByteGPU sumbyte = new TestSumByteGPU();
  TestMatrixCopyGPU matrix = new TestMatrixCopyGPU();


  @Test
  public void testAll() {
    for (int i = 0; i < ITERATIONS; i++) {
      prefetch.testPrefetchCopyGPU();
      prefetch.testNoPrefetchCopyGPU();
      sum.testPackedSumGPU();
      sum.testUnpackedSumGPU();
      sumbyte.testPackedSumByteGPU();
      sumbyte.testUnpackedSumByteGPU();
      matrix.testMatrixCopyXYGPU();
      matrix.testMatrixCopyYXGPU();
    }
  }
}
