package ca.uwaterloo.gpuclu;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;

import org.jocl.Sizeof;
import org.jocl.cl_event;
import org.junit.Test;

public class TestMatrixCopyGPU {

  @Test
  public void testMatrixCopyXYGPU() {
    final OpenCLComputation c = new TestMatrixCopyXYComputation(8192);
    final OpenCLComputer computer = new OpenCLComputer(true, true, c);
    computer.initialize();
    computer.compute();
    computer.release();
  }

  @Test
  public void testMatrixCopyYXGPU() {
    final OpenCLComputation c = new TestMatrixCopyYXComputation(8192);
    final OpenCLComputer computer = new OpenCLComputer(true, true, c);
    computer.initialize();
    computer.compute();
    computer.release();
  }

  class TestMatrixCopyXYComputation implements OpenCLComputation {

    final static boolean prefetch = false;
    final static int workgroupSize = 32;

    private final static String kernel_file_matrix_copy_xy =
        "test/ca/uwaterloo/gpuclu/matrix_copy_XY.cl";

    final Mem[] mems;
    final Dim[] dims;

    final int dim_length;
    final int array_size;

    final MemInt linear_matrix;
    final MemInt output;

    TestMatrixCopyXYComputation(final int d) {
      this.dim_length = d;
      this.array_size = d * d;
      this.linear_matrix =
          new MemInt("linear_matrix", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> array_size));
      this.output =
          new MemInt("unpacked_output", MemConfig.DownloadOnly, new MemCapacity(() -> array_size));
      this.mems = new Mem[] {linear_matrix, output};
      this.dims = new Dim[] {new Dim(0, dim_length), new Dim(1, dim_length)};
    }

    @Override
    public Mem[] memories() {
      return mems;
    }

    @Override
    public Dim[] dimensions() {
      return dims;
    }

    @Override
    public Kernel[] kernels() {
      return kernels;
    }

    final Kernel[] kernels =
        new Kernel[] {new Kernel(kernel_file_matrix_copy_xy, "matrix_copy_XY") {

          @Override
          public cl_event prepareLaunchInner() {
            setArg("linear_matrix", Sizeof.cl_mem, linear_matrix.d_pointer());
            setArg("length", Sizeof.cl_int, OpenCLUtil.pointerToInt(dim_length));
            setArg("output", Sizeof.cl_mem, output.d_pointer());

            // launch matrix_copy kernel
            final cl_event kernel_execution_event = new cl_event();
            final int work_items = array_size;
            total_work_items += work_items;

            work_dim = dims.length;
            global_work_offset = null;
            global_work_size = new long[] {dim_length, dim_length};
            local_work_size = null;
            num_events_in_wait_list = 0;
            event_wait_list = null;
            event = kernel_execution_event;

            return kernel_execution_event;
          }
        }};


    @Override
    public void uploadPreprocessing() {
      output.h[0] = 0;
      for (int i = 0; i < array_size; i++) {
        linear_matrix.h[i] = 1;
      }
      try {
        handler = new FileHandler("matrix_copy_xy_log%u.txt");
      } catch (final IOException e) {
        System.out.println("Could not create file. Using the console handler");
      }
      logger.addHandler(handler);
    }

    @Override
    public void uploadPostprocessing() {}

    @Override
    public void downloadPreprocessing() {}

    @Override
    public void downloadPostprocessing() {}

    @Override
    public Logger logger() {
      return logger;
    }

    private final Logger logger = Logger.getLogger(TestMatrixCopyXYComputation.class
        .getSimpleName());

    private Handler handler;
  }

  class TestMatrixCopyYXComputation implements OpenCLComputation {

    final static boolean prefetch = false;
    final static int workgroupSize = 32;

    private final static String kernel_file_matrix_copy_yx =
        "test/ca/uwaterloo/gpuclu/matrix_copy_YX.cl";

    final Mem[] mems;
    final Dim[] dims;

    final int dim_length;
    final int array_size;

    final MemInt linear_matrix;
    final MemInt output;

    TestMatrixCopyYXComputation(final int d) {
      this.dim_length = d;
      this.array_size = d * d;
      this.linear_matrix =
          new MemInt("linear_matrix", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> array_size));
      this.output =
          new MemInt("unpacked_output", MemConfig.DownloadOnly, new MemCapacity(() -> array_size));
      this.mems = new Mem[] {linear_matrix, output};
      this.dims = new Dim[] {new Dim(0, dim_length), new Dim(1, dim_length)};
    }

    @Override
    public Mem[] memories() {
      return mems;
    }

    @Override
    public Dim[] dimensions() {
      return dims;
    }

    @Override
    public Kernel[] kernels() {
      return kernels;
    }

    final Kernel[] kernels =
        new Kernel[] {new Kernel(kernel_file_matrix_copy_yx, "matrix_copy_YX") {

          @Override
          public cl_event prepareLaunchInner() {
            setArg("linear_matrix", Sizeof.cl_mem, linear_matrix.d_pointer());
            setArg("length", Sizeof.cl_int, OpenCLUtil.pointerToInt(dim_length));
            setArg("output", Sizeof.cl_mem, output.d_pointer());

            // launch matrix_copy kernel
            final cl_event kernel_execution_event = new cl_event();
            final int work_items = array_size;
            total_work_items += work_items;

            work_dim = dims.length;
            global_work_offset = null;
            global_work_size = new long[] {dim_length, dim_length};
            local_work_size = null;
            num_events_in_wait_list = 0;
            event_wait_list = null;
            event = kernel_execution_event;

            return kernel_execution_event;
          }
        }};


    @Override
    public void uploadPreprocessing() {
      output.h[0] = 0;
      for (int i = 0; i < array_size; i++) {
        linear_matrix.h[i] = 1;
      }
      try {
        handler = new FileHandler("matrix_copy_yx_log%u.txt");
      } catch (final IOException e) {
        System.out.println("Could not create file. Using the console handler");
      }
      logger.addHandler(handler);
    }

    @Override
    public void uploadPostprocessing() {}

    @Override
    public void downloadPreprocessing() {}

    @Override
    public void downloadPostprocessing() {}

    @Override
    public Logger logger() {
      return logger;
    }

    private final Logger logger = Logger.getLogger(TestMatrixCopyYXComputation.class
        .getSimpleName());
    private Handler handler;
  }

}
