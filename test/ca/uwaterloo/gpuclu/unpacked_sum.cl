__kernel void unpacked_sum(
	__global const uint* const unpacked_input, 
	__global uint* const sum)
{
    const int tid = get_global_id(0);

    const int bit = unpacked_input[tid];

	atomic_add(&sum[0], bit);
}
