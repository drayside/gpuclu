__kernel void no_prefetch_copy(
	__global const uint* const input, 
	const uint g_input_cell_count,
	__global uint* const output)
{
    const int workID = get_global_id(0);
	output[workID] = input_get(input, workID);
}

