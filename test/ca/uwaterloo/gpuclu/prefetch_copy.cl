__kernel void prefetch_copy(
	__global const uint* const g_input,
	const uint g_input_cell_count,
	__global uint* const output)
{
    const int workID = get_global_id(0);

	INIT_VAR_input(g_input, input)
	output[workID] = input_get(input, workID);
}
