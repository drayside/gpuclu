package ca.uwaterloo.gpuclu;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;

import org.jocl.Sizeof;
import org.jocl.cl_event;
import org.junit.Test;

public class TestSumGPU {

  @Test
  public void testPackedSumGPU() {
    final PackedArrayImpl a = new PackedArrayImpl(2, 1024 * 1024 * 64);
    final OpenCLComputation c = new TestPackedSumComputation(a);
    final OpenCLComputer computer = new OpenCLComputer(true, true, c);
    computer.initialize();
    computer.compute();
    computer.release();
  }

  @Test
  public void testUnpackedSumGPU() {
    final OpenCLComputation c = new TestUnpackedSumComputation(1024 * 1024 * 64);
    final OpenCLComputer computer = new OpenCLComputer(true, true, c);
    computer.initialize();
    computer.compute();
    computer.release();
  }

  class TestPackedSumComputation implements OpenCLComputation {

    final static boolean prefetch = false;
    final static int workgroupSize = 32;

    private final static String kernel_file_packed_sum = "test/ca/uwaterloo/gpuclu/packed_sum.cl";

    final PackedArrayImpl p;

    final Mem[] mems;
    final Dim[] dims;

    final MemPacked packed_input;
    final MemInt packed_output;

    TestPackedSumComputation(final PackedArrayImpl p) {
      this.p = p;
      this.packed_input =
          new MemPacked("packed_input", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> p.cellCount()), p.config.bitwidth, prefetch, workgroupSize, p.cellCount());
      this.packed_output =
          new MemInt("packed_output", MemConfig.DownloadOnly, new MemCapacity(() -> 1));
      this.mems = new Mem[] {packed_input, packed_output};
      this.dims = new Dim[] {new Dim(0, packed_input.logicalCapacity())};
    }

    @Override
    public Mem[] memories() {
      return mems;
    }

    @Override
    public Dim[] dimensions() {
      return dims;
    }

    @Override
    public Kernel[] kernels() {
      return kernels;
    }

    final Kernel[] kernels = new Kernel[] {new Kernel(kernel_file_packed_sum, "packed_sum") {

      @Override
      public cl_event prepareLaunchInner() {
        setArg("packed_input", Sizeof.cl_mem, packed_input.d_pointer());
        setArg("sum", Sizeof.cl_mem, packed_output.d_pointer());

        // launch packed_sum kernel
        final cl_event kernel_execution_event = new cl_event();
        final int work_items = dims[0].size; // packed_input.logicalCapacity();
        total_work_items += work_items;

        work_dim = dims.length;
        global_work_offset = null;
        global_work_size = new long[] {work_items};
        local_work_size = null;
        num_events_in_wait_list = 0;
        event_wait_list = null;
        event = kernel_execution_event;

        return kernel_execution_event;
      }
    }};


    @Override
    public void uploadPreprocessing() {
      packed_output.h[0] = 0;
      for (int i = 0; i < packed_input.logicalCapacity(); i++) {
        packed_input.h_append(1);
      }
      try {
        handler = new FileHandler("packed_sum_log%u.txt");
      } catch (final IOException e) {
        System.out.println("Could not create file. Using the console handler");
      }
      logger.addHandler(handler);
    }

    @Override
    public void uploadPostprocessing() {}

    @Override
    public void downloadPreprocessing() {}

    @Override
    public void downloadPostprocessing() {
      assert packed_output.h[0] == dims[0].size;
    }

    @Override
    public Logger logger() {
      return logger;
    }

    private final Logger logger = Logger.getLogger(TestPackedSumComputation.class.getSimpleName());
    private Handler handler;
  }

  class TestUnpackedSumComputation implements OpenCLComputation {

    final static boolean prefetch = false;
    final static int workgroupSize = 32;

    private final static String kernel_file_unpacked_sum =
        "test/ca/uwaterloo/gpuclu/unpacked_sum.cl";

    int length;

    final Mem[] mems;
    final Dim[] dims;

    final MemInt unpacked_input;
    final MemInt unpacked_output;

    TestUnpackedSumComputation(final int l) {
      this.length = l;
      this.unpacked_input =
          new MemInt("unpacked_input", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> length));
      this.unpacked_output =
          new MemInt("unpacked_output", MemConfig.DownloadOnly, new MemCapacity(() -> 1));
      this.mems = new Mem[] {unpacked_input, unpacked_output};
      this.dims = new Dim[] {new Dim(0, length)};
    }

    @Override
    public Mem[] memories() {
      return mems;
    }

    @Override
    public Dim[] dimensions() {
      return dims;
    }

    @Override
    public Kernel[] kernels() {
      return kernels;
    }

    final Kernel[] kernels = new Kernel[] {new Kernel(kernel_file_unpacked_sum, "unpacked_sum") {

      @Override
      public cl_event prepareLaunchInner() {
        setArg("unpacked_input", Sizeof.cl_mem, unpacked_input.d_pointer());
        setArg("sum", Sizeof.cl_mem, unpacked_output.d_pointer());

        // launch packed_sum kernel
        final cl_event kernel_execution_event = new cl_event();
        final int work_items = dims[0].size;
        total_work_items += work_items;

        work_dim = dims.length;
        global_work_offset = null;
        global_work_size = new long[] {work_items};
        local_work_size = null;
        num_events_in_wait_list = 0;
        event_wait_list = null;
        event = kernel_execution_event;

        return kernel_execution_event;
      }
    }};


    @Override
    public void uploadPreprocessing() {
      unpacked_output.h[0] = 0;
      for (int i = 0; i < dims[0].size; i++) {
        unpacked_input.h[i] = 1;
      }
      try {
        handler = new FileHandler("unpacked_sum_log%u.txt");
      } catch (final IOException e) {
        System.out.println("Could not create file. Using the console handler");
      }
      logger.addHandler(handler);
    }

    @Override
    public void uploadPostprocessing() {}

    @Override
    public void downloadPreprocessing() {}

    @Override
    public void downloadPostprocessing() {
      assert unpacked_output.h[0] == dims[0].size;
    }

    @Override
    public Logger logger() {
      return logger;
    }

    private final Logger logger = Logger
        .getLogger(TestUnpackedSumComputation.class.getSimpleName());

    private Handler handler;
  }

}
