package ca.uwaterloo.gpuclu;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Random;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;

import org.jocl.Sizeof;
import org.jocl.cl_event;
import org.junit.Test;

public class TestPrefetchCopyGPU {

  private static final int SIZE = 512;

  @Test
  public void testPrefetchCopyGPU() {
    final PackedArrayImpl a = new PackedArrayImpl(4, SIZE);
    final OpenCLComputation c = new TestPrefetchCopyComputation(a);
    final OpenCLComputer computer = new OpenCLComputer(true, true, c);
    computer.initialize();
    computer.compute();
    computer.release();
  }

  @Test
  public void testNoPrefetchCopyGPU() {
    final PackedArrayImpl a = new PackedArrayImpl(4, SIZE);
    final OpenCLComputation c = new TestNoPrefetchCopyComputation(a);
    final OpenCLComputer computer = new OpenCLComputer(true, true, c);
    computer.initialize();
    computer.compute();
    computer.release();
  }

  class TestPrefetchCopyComputation implements OpenCLComputation {

    final static int workgroupSize = 32;

    private final static String kernel_file_copy_prefetch =
        "test/ca/uwaterloo/gpuclu/prefetch_copy.cl";

    final PackedArrayImpl p;

    final Mem[] mems;
    final Dim[] dims;

    final MemPacked input;
    final MemInt unpacked_output;

    TestPrefetchCopyComputation(final PackedArrayImpl p) {
      this.p = p;
      this.input =
          new MemPacked("input", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> p.cellCount()), p.config.bitwidth, true, workgroupSize, 4);
      this.unpacked_output =
          new MemInt("unpacked_output", MemConfig.DownloadOnly, new MemCapacity(() -> p.capacity()));
      this.mems = new Mem[] {input, unpacked_output};
      this.dims = new Dim[] {new Dim(0, input.logicalCapacity())};
    }

    @Override
    public Mem[] memories() {
      return mems;
    }

    @Override
    public Dim[] dimensions() {
      return dims;
    }

    @Override
    public Kernel[] kernels() {
      return kernels;
    }

    final Kernel[] kernels = new Kernel[] {new Kernel(kernel_file_copy_prefetch, "prefetch_copy") {

      @Override
      public cl_event prepareLaunchInner() {
        setArg("input", Sizeof.cl_mem, input.d_pointer());
        setArg("input_cell_count", Sizeof.cl_int, OpenCLUtil.pointerToInt(input.h_length()));
        setArg("output", Sizeof.cl_mem, unpacked_output.d_pointer());

        // launch prefetch_copy kernel
        final cl_event kernel_execution_event = new cl_event();
        final int work_items = dims[0].size;
        total_work_items += work_items;

        work_dim = dims.length;
        global_work_offset = null;
        global_work_size = new long[] {work_items};
        local_work_size = new long[] {workgroupSize}; // null;
        num_events_in_wait_list = 0;
        event_wait_list = null;
        event = kernel_execution_event;

        return kernel_execution_event;
      }
    }};


    @Override
    public void uploadPreprocessing() {
      final Random r = new Random();
      for (int i = 0; i < input.logicalCapacity(); i++) {
        final int val = r.nextInt(16);
        p.append(val);
        input.h_append(val);
      }
      try {
        handler = new FileHandler("prefetch_copy_log%u.txt");
      } catch (final IOException e) {
        System.out.println("Could not create file. Using the console handler");
      }
      logger.addHandler(handler);
    }

    @Override
    public void uploadPostprocessing() {}

    @Override
    public void downloadPreprocessing() {}

    @Override
    public void downloadPostprocessing() {
      for (int i = 0; i < input.logicalCapacity(); i++) {
        assertEquals(p.get(i), unpacked_output.h[i]);
      }
    }

    @Override
    public Logger logger() {
      return logger;
    }

    private final Logger logger = Logger.getLogger(TestPrefetchCopyComputation.class
        .getSimpleName());
    private Handler handler;

  }

  class TestNoPrefetchCopyComputation implements OpenCLComputation {

    final static int workgroupSize = 32;

    private final static String kernel_file_copy_no_prefetch =
        "test/ca/uwaterloo/gpuclu/no_prefetch_copy.cl";

    final PackedArrayImpl p;

    final Mem[] mems;
    final Dim[] dims;

    final MemPacked input;
    final MemInt unpacked_output;

    TestNoPrefetchCopyComputation(final PackedArrayImpl p) {
      this.p = p;
      this.input =
          new MemPacked("input", MemConfig.IncrementalUploadOnly, new MemCapacity(
              () -> p.cellCount()), p.config.bitwidth, false, workgroupSize, p.cellCount());
      this.unpacked_output =
          new MemInt("unpacked_output", MemConfig.DownloadOnly, new MemCapacity(() -> p.capacity()));
      this.mems = new Mem[] {input, unpacked_output};
      this.dims = new Dim[] {new Dim(0, input.logicalCapacity())};
    }

    @Override
    public Mem[] memories() {
      return mems;
    }

    @Override
    public Dim[] dimensions() {
      return dims;
    }

    @Override
    public Kernel[] kernels() {
      return kernels;
    }

    final Kernel[] kernels = new Kernel[] {new Kernel(kernel_file_copy_no_prefetch,
        "no_prefetch_copy") {

      @Override
      public cl_event prepareLaunchInner() {
        setArg("input", Sizeof.cl_mem, input.d_pointer());
        setArg("input_cell_count", Sizeof.cl_int, OpenCLUtil.pointerToInt(input.h_length()));
        setArg("output", Sizeof.cl_mem, unpacked_output.d_pointer());

        // launch no_prefetch_copy kernel
        final cl_event kernel_execution_event = new cl_event();
        final int work_items = dims[0].size;
        total_work_items += work_items;

        work_dim = dims.length;
        global_work_offset = null;
        global_work_size = new long[] {work_items};
        local_work_size = null;
        num_events_in_wait_list = 0;
        event_wait_list = null;
        event = kernel_execution_event;

        return kernel_execution_event;
      }
    }};


    @Override
    public void uploadPreprocessing() {
      final Random r = new Random();
      for (int i = 0; i < input.logicalCapacity(); i++) {
        final int val = r.nextInt(16);
        p.append(val);
        input.h_append(val);
      }
      try {
        handler = new FileHandler("no_prefetch_log%u.txt");
      } catch (final IOException e) {
        System.out.println("Could not create file. Using the console handler");
      }
      logger.addHandler(handler);
    }

    @Override
    public void uploadPostprocessing() {}

    @Override
    public void downloadPreprocessing() {}

    @Override
    public void downloadPostprocessing() {
      for (int i = 0; i < input.logicalCapacity(); i++) {
        assertEquals(p.get(i), unpacked_output.h[i]);
      }
    }

    @Override
    public Logger logger() {
      return logger;
    }

    private final Logger logger = Logger.getLogger(TestNoPrefetchCopyComputation.class
        .getSimpleName());
    private Handler handler;
  }

}
