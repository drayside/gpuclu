__kernel void prefetch_sum(
	__global const uint* const input,
	const uint g_input_cell_count,
	__global uint* const output)
{
	for(uint index = 0; index < g_input_cell_count; index++)
		output[workID] += input[index];
	input[6] = 6;
	gr = input[6];
}