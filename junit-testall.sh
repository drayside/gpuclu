#!/bin/bash

java \
    -ea \
    -Djava.util.logging.config.file="logging.properties" \
    -cp bin:/usr/share/java/junit4.jar:lib/JOCL-0.1.9.jar \
    org.junit.runner.JUnitCore \
    ca.uwaterloo.gpuclu.TestAll

g++ ParseLogs.cpp -std=c++0x -o parser

./parser

python printGraphs.py

rm *.txt*
