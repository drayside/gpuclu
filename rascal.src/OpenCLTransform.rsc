module OpenCLTransform
import IO;
import String;
import List;
import ParseTree;
import OpenCLGrammar;

//Test kernel : project://gpuclu/test/prefetch.c

//To prefetch the input "input" into the 1-dimensional kernel located in project://gpuclu/test/prefetch.c, do
//main(["input"], |project://gpuclu/test/prefetch.c|, 1);
str main(list[str] prefetchMems, loc kernelFile, int kernelDims) {
    Tree t = parse(#TranslationUnit, readFile(kernelFile));
    t2 = bottom-up visit(t){
           case (Declarator) `<Declarator v>` => (unparse(v) in prefetchMems) ? parse(#Declarator, "<v>_global") : v
           case (Expression) `<Expression a> [ <Expression i> ] = <Expression b>` => parse(#Expression, "<a>_set(<a>, <i>, <b>)")
           case (Expression) `<Expression a> [ <Expression i> ] += <Expression b>` => parse(#Expression, "<a>_set(<a>, <i>, <b> + <a>_get(<a>,<i>))")
           case (Expression) `<Expression a> [ <Expression i> ] -= <Expression b>` => parse(#Expression, "<a>_set(<a>, <i>, <b> - <a>_get(<a>,<i>))")
           case (Expression) `<Expression a> [ <Expression i> ] *= <Expression b>` => parse(#Expression, "<a>_set(<a>, <i>, <b> * <a>_get(<a>,<i>))")
           case (Expression) `<Expression a> [ <Expression i> ] /= <Expression b>` => parse(#Expression, "<a>_set(<a>, <i>, <b> / <a>_get(<a>,<i>))")
           case (Expression) `<Expression a> [ <Expression i> ] %= <Expression b>` => parse(#Expression, "<a>_set(<a>, <i>, <b> % <a>_get(<a>,<i>))")
           case (Expression) `<Expression a> [ <Expression i> ] ++` => parse(#Expression, "<a>_set(<a>, <i>, <a>_get(<a>,<i>) + 1)")
           case (Expression) `<Expression a> [ <Expression i> ] --` => parse(#Expression, "<a>_set(<a>, <i>, <a>_get(<a>,<i>) - 1)")
    };
    t3 = bottom-up visit(t2){
           case (Expression)`<Expression a> [ <Expression i> ]` => parse(#Expression, "<a>_get(<a>, <i>)")
    };
    
    str declCode = "";
    
    str dims = "";
    if(kernelDims == 2)
    		    dims = "2D_";
    
    for(int i <- [0 .. size(prefetchMems)]) {
        str name = prefetchMems[i];
    	    declCode += "INIT_VAR_" + dims + name + "(" + name + "_global, " + name + ")\n\t";						
    }					

	kernelCode = unparse(t3);
	kernelCode = replaceFirst(kernelCode, "{", "{\n\t" + declCode);
	
	//println(kernelCode);
    return kernelCode;
}