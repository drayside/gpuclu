package ca.uwaterloo.gpuclu;



public abstract class PackedArray {

  public PackedArray() {
    super();
  }


  /** The number of logical elements in this PackedArray. */
  public abstract int size();

  /** The logical capacity of this PackedArray. */
  public abstract int capacity();

  /**
   * The maximum value that can be stored in this packed array. If the bitwidth is 32 this will say
   * Integer.MAX_VALUE (which is only 31 bits).
   */
  public abstract int maxValue();

  public abstract void set(final int index, final int value);

  public abstract int get(int index);

  @Override
  public String toString() {
    final StringBuffer b = new StringBuffer();
    b.append('[');
    if (size() > 0) {
      b.append(get(0));
    }
    for (int i = 1; i < size(); i++) {
      b.append(',');
      b.append(get(i));
    }
    b.append(']');
    return b.toString();
  }

}
