// zero out an array of integers
__kernel void zero_int(__global int* const a)
{
    const int tid = get_global_id(0);
    a[tid] = 0;
}