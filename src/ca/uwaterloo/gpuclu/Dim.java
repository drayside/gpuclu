package ca.uwaterloo.gpuclu;

public class Dim {

  // used in kernel (OpenCL code)
  public final int deviceDim;

  // used in kernel launch (Java code)
  public final int size;

  public Dim(final int deviceDim, final int size) {
    super();
    this.deviceDim = deviceDim;
    this.size = size;
  }



}
