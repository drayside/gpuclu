package ca.uwaterloo.gpuclu;

/**
 * Provides a view on a sublist of some backing PackedArray.
 *
 */
public class PackedArraySection extends PackedArray {

  private final int startIndex;

  private final int requestedIndicesPerSection;
  private final int actualIndicesPerSection;

  private final PackedArray delegate;


  public PackedArraySection(final PackedArray delegate, final int startIndex,
      final int requestedIndicesPerSection, final int actualIndicesPerSection) {
    this.delegate = delegate;
    this.startIndex = startIndex;
    this.actualIndicesPerSection = actualIndicesPerSection;
    this.requestedIndicesPerSection = requestedIndicesPerSection;
    assert startIndex + actualIndicesPerSection <= delegate.capacity() : "backing PackedArray does not have capacity to support this view";
  }

  @Override
  public int size() {
    return requestedIndicesPerSection;
  }

  @Override
  public int capacity() {
    return actualIndicesPerSection;
  }

  @Override
  public int maxValue() {
    return delegate.maxValue();
  }

  @Override
  public void set(final int index, final int value) {
    // index into the backing PackedArray
    assert index < actualIndicesPerSection;
    delegate.set(startIndex + index, value);
  }

  @Override
  public int get(final int index) {
    // index into the backing PackedArray
    assert index < actualIndicesPerSection : "index exceeds sectionCapacity: "
        + Util.i2s(index, actualIndicesPerSection);
    return delegate.get(startIndex + index);
  }

}
