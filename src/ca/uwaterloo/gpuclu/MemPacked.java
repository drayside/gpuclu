package ca.uwaterloo.gpuclu;

import org.jocl.Pointer;
import org.jocl.Sizeof;
import org.jocl.cl_kernel;


public final class MemPacked extends Mem {

  public static MemPacked EMPTY_MEM_PACKED() {
    return new MemPacked("EMPTY_MEM", MemConfig.IncrementalPersistentState,
        new MemCapacity(() -> 1), 32, false, 1, 1);
  }


  static cl_kernel zero_kernel = null;

  public PackedArrayImpl h;
  public final PackedArrayConfig packedArrayConfig;

  private PackedArrayImpl leftovers;
  private int numValuesUploadedTotal;
  private int numValuesUploadedTwice;
  private int numValuesUploadedTwicePending;

  private final boolean prefetch;
  private final int workgroupsize;


  public MemPacked(final String name, final MemConfig config, final MemCapacity capacity,
      final int requestedBitwidth, final boolean prefetch, final int workgroupsize,
      final int cellsPerSection) {
    super(name, config, capacity);

    final int sectionCount = (int) Math.ceil(capacity.hostCapacity() / (double) cellsPerSection);
    final PackedArrayConfig a = new PackedArrayConfig(name, requestedBitwidth);
    final int indicesPerSection = cellsPerSection * a.indicesPerCell;
    this.packedArrayConfig =
        new PackedArrayConfig(name, requestedBitwidth, sectionCount, indicesPerSection);

    this.leftovers = new PackedArrayImpl(name + "_leftovers", requestedBitwidth, 0);
    this.prefetch = prefetch;
    this.workgroupsize = workgroupsize;
    // this.cellsPerSection = cellsPerSection;
    // this.indicesPerSection = cellsPerSection * valuesPerCell;
  }

  @Override
  public int deviceSizeUnit() {
    return Sizeof.cl_int;
  }

  @Override
  public void initHost() {
    // initialize mem on CPU
    final int h_capacity = logicalCapacity();
    if (h_capacity <= 0) {
      // this will be set dynamically by the client
      h = null;
    } else {
      // this is a static buffer that we initialize here
      initHost(h_capacity);
    }
  }

  public int logicalCapacity() {
    return capacity.hostCapacity() * packedArrayConfig.indicesPerCell;
  }

  public void initHost(final int logicalCapacity) {
    // initialize mem on CPU
    assert capacity.hostCapacity() > 0 : "capacity is pre-determined, cannot be set dynamically";

    // System.out.println("initHost: " + leftover.size());
    h = new PackedArrayImpl(packedArrayConfig);
    for (int i = 0; i < leftovers.size(); i++) {
      final int x = leftovers.get(i);
      // System.out.println("packing leftover: " + x);
      h.set(i, x);
      numValuesUploadedTwicePending++;
    }
    // leftover is now stale
    // we keep it around to compute d_offset()
  }

  /** The number of logical values uploaded. */
  public int num_values_uploaded() {
    return numValuesUploadedTotal - numValuesUploadedTwice;
  }

  public void h_append(final int value) {
    h.append(value);
  }

  @Override
  public int h_length() {
    return (null == h) ? 0 : h.cellCount();
  }

  @Override
  public boolean h_isNull() {
    return null == h;
  }

  @Override
  public void h_clear() {
    if (null != h) {
      // leftover should not be cleared here
      initHost(h.cellCount());
    }
  }

  @Override
  public Pointer h_pointer() {
    return Pointer.to(h.getCells());
  }

  @Override
  public void h_downloadPostprocessing() {
    if (config.download) {
      h.recalibrateSize();
    }
  }

  @Override
  protected int d_offset() {
    if (config.incremental_upload) {
      return PackedArrayImpl.cellCount(num_values_uploaded(), packedArrayConfig.indicesPerCell)
          - (leftovers.size() > 0 ? 1 : 0);
    } else {
      return 0;
    }
  }

  @Override
  public String getOpenCLCode() {
    return packedArrayConfig.generateOpenCLCode(prefetch, workgroupsize);
  }

  @Override
  public void releaseHost() {
    // this is called just after upload and at the end to release resources
    // TODO: separate these two distinct uses of this method
    if (null != h) {
      numValuesUploadedTotal += h.size();
      numValuesUploadedTwice += numValuesUploadedTwicePending;
      numValuesUploadedTwicePending = 0;
      leftovers = h.getLeftovers();
      // System.out.println("releaseHost: " + Util.i2s(h.size(), leftover.size()));
      if (config.download) {
        h = null;
      }
    }
  }

  @Override
  public cl_kernel d_zero_kernel() {
    return zero_kernel;
  }

}
