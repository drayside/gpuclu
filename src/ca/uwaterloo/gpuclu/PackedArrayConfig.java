package ca.uwaterloo.gpuclu;


public final class PackedArrayConfig {

  /**
   * The name of this array, used for generating OpenCL code. Should not collide with any other name
   * that will go into the same OpenCL code.
   */
  public final String name;


  /** Number of bits used for each logical value. Must be a power of 2. */
  public final int bitwidth;

  protected final int bitwidthLog2;

  /** Number of logical values that can be stored in a cell. */
  public final int indicesPerCell;

  protected final int indicesPerCellLog2;

  protected final int indexMask;

  /** Mask to extract values. */
  public final int valueMask;

  /** Number of indices available (might include padding). */
  protected final int indexCount;
  // TODO: Should this be the number of indices we want? (excluding padding)


  /** Number of cells. */
  protected final int cellCount;

  /** Number of sections that this PackedArray is chunked into. */
  protected final int sectionCount;

  /**
   * Number of logical indices the user wishes to address per section. The number of actual indices
   * per section might be larger due to padding.
   */
  protected final int requestedIndicesPerSection;

  /** Number of cells in each section (which might incorporate padding). */
  protected final int cellsPerSection;

  public PackedArrayConfig(final String name, final int requestedBitwidth) {
    this(name, requestedBitwidth, 1, 1);
  }

  public PackedArrayConfig(final String name, final int requestedBitwidth, final int sectionCount,
      final int requestedIndicesPerSection) {
    super();
    this.name = name;

    if (requestedBitwidth <= 0) {
      throw new IllegalArgumentException("requestedBitWidth must be greater than 0");
    }
    if (requestedBitwidth > 32) {
      throw new IllegalArgumentException("requestedBitWidth must be less than or equal to 32");
    }
    if (sectionCount < 0) {
      throw new IllegalArgumentException("sectionCount must be greater than or equal to 0");
    }
    if (requestedIndicesPerSection < 0) {
      throw new IllegalArgumentException(
          "requestedIndicesPerSection must be greater than or equal to 0");
    }

    // determine bitwidth
    if ((32 % requestedBitwidth) == 0) {
      this.bitwidth = requestedBitwidth;
    } else {
      // requested bitwidth does not divide evenly into a cell
      // increase it until it does
      this.bitwidth = 1 << (log2(requestedBitwidth) + 1);
    }
    assert (32 % bitwidth) == 0;
    this.indicesPerCell = 32 / bitwidth;
    this.indicesPerCellLog2 = log2(indicesPerCell);
    this.indexMask = createMask(indicesPerCellLog2);
    this.bitwidthLog2 = log2(bitwidth);
    this.valueMask = createMask(bitwidth);

    // determine section parameters
    this.sectionCount = sectionCount;
    this.requestedIndicesPerSection = requestedIndicesPerSection;
    this.cellsPerSection = (int) Math.ceil(requestedIndicesPerSection / (double) indicesPerCell);

    this.cellCount = sectionCount * cellsPerSection;
    this.indexCount = cellCount * indicesPerCell;

  }

  public static int createMask(final int width) {
    if (width == 32) {
      return -1;
    }
    return (1 << width) - 1;
  }

  public static int log2(final int x) {
    // log2 is the same as finding the most significant bit
    // https://graphics.stanford.edu/~seander/bithacks.html#IntegerLogObvious
    int v = x;
    int r = 0;
    while ((v >>= 1) != 0) {
      r++;
    }
    return r;
  }

  public String generateOpenCLCode(final boolean prefetch, final int workgroupsize) {
    final StringBuffer b = new StringBuffer(2000);

    // constants
    line("#define name_bitwidth " + bitwidth, b);
    line("#define name_bitwidthLog2 " + bitwidthLog2, b);
    line("#define name_valuesPerCell " + indicesPerCell, b);
    line("#define name_valuesPerCellLog2 " + indicesPerCellLog2, b);
    line("#define name_valueMask 0x" + Integer.toHexString(valueMask), b);
    line("#define name_indexMask 0x" + Integer.toHexString(indexMask), b);
    if (prefetch) {
      // line("#define assignments_scope __local", b);
      line("#define name_base_offset 0", b);
    } else {
      // line("#define name_scope const __global", b);
      line("#define name_base_offset NUM_VARIABLES_ALIGNED", b);
    }

    // accessor functions
    line("uint name_cell(const uint index) { return index >> name_valuesPerCellLog2; }", b);
    line(
        "uint name_subcell(const uint index) { return (index & name_indexMask) << name_bitwidthLog2; }",
        b);
    line(
        "int name_get2(SCOPE const uint* const a, const uint cell, const uint subcell) { return (a[cell] >> subcell) & name_valueMask; }",
        b);

    // mutator functions

    if (bitwidth == 32) {
      line(
          "void name_set(SCOPE uint* const a, const uint index, const int v) { atomic_cmpxchg(&a[index], a[index], v); }",
          b);
    } else {
      line(
          "void name_set2(SCOPE uint* const a, const uint cell, const uint subcell, const int v) { "
              // the code we need to re-write in a thread-safe manner
              // "/* a[cell] &= ~(name_valueMask << subcell); a[cell] |= (v << subcell); */ "
              // lock-free looping
              + "uint original = 0; uint current = 0; " // initial values
              + "do { " // keep looping until we win
              + "    original = a[cell]; "
              + "    const uint cleared = original & ~(name_valueMask << subcell); "
              + "    const uint desired = cleared | v << subcell; "
              + "    current = atomic_cmpxchg(&a[cell], original, desired); "
              + "} while (current != original);" + "}", b);
      line(
          "void name_set(SCOPE uint* const a, const uint index, const uint v) { name_set2(a, name_cell(index), name_subcell(index), v); }",
          b);
    }

    // initialize local variable name to use prefetching (or not)
    if (prefetch) {
      line(
          "int name_get(SCOPE const uint* const a, const uint index) { const uint x = index % INDICES_PER_SECTION; return name_get2(a, name_cell(x), name_subcell(x)); }",
          b);
      line("#define LINEAR_LOCAL_ID_name  (get_local_id(1) * get_local_size(0) + get_local_id(0))",
          b);
      line("#define LINEAR_GROUP_ID_name  (get_group_id(1) * get_num_groups(0) + get_group_id(0))",
          b);
      line(
          "#define LINEAR_GLOBAL_ID_name  (LINEAR_GROUP_ID_name * WORKGROUPSIZE + LINEAR_LOCAL_ID_name)",
          b);
      line(
          "uint name_getSectionID() { return (get_group_id(0) * NUMBER_OF_SECTIONS) / get_num_groups(0); }",
          b);
      line("uint name_getSectionID_2D() { return LINEAR_GROUP_ID_name % NUMBER_OF_SECTIONS;}", b);
      line("#define INIT_VAR_name(global,local) __local uint (local)[CELLS_PER_SECTION]; "
          + "prefetch_name(name_getSectionID(), get_local_id(0), (global), (local));", b);
      line("#define INIT_VAR_2D_name(global,local) __local uint (local)[CELLS_PER_SECTION]; "
          + "prefetch_name(name_getSectionID_2D(), LINEAR_LOCAL_ID_name, (global), (local));", b);
      line(
          "#define INIT_VAR_2D_row_major_name(global,local, tileID, M) __local uint (local)[WORKGROUPSIZE][WORKGROUPSIZE]; "
              + "prefetch_2D_row_major_name(tileID, M, (global), (local));", b);
      line(
          "#define INIT_VAR_2D_col_major_name(global,local, tileID, K) __local uint (local)[WORKGROUPSIZE][WORKGROUPSIZE]; "
              + "prefetch_2D_col_major_name(tileID, K, (global), (local));", b);
    } else {
      line(
          "int name_get(SCOPE const uint* const a, const uint index) { return name_get2(a, name_cell(index), name_subcell(index)); }",
          b);
      line("#define INIT_VAR_name(global,local) __global const uint* (local) = (global);", b);
      line("#define INIT_VAR_2D_name(global,local) __global const uint* (local) = (global);", b);
      line(
          "#define INIT_VAR_2D_row_major_name(global,local, tileID, M) __global const uint* (local) = (global);",
          b);
      line(
          "#define INIT_VAR_2D_col_major_name(global,local, tileID, K) __global const uint* (local) = (global);",
          b);
    }

    // prefetch functions
    final int loopBoundFloor = Math.floorDiv(cellsPerSection, workgroupsize);
    final int loopBoundCeil = (int) Math.ceil((float) cellsPerSection / workgroupsize);
    if (prefetch) {

      // translate section index to global index
      line("uint name_global_index(uint sectionID, uint local_index) {", b);
      line("  return (sectionID * INDICES_PER_SECTION + local_index);", b);
      line("}", b);

      line(
          "void prefetch_name(const uint sectionID, const uint threadID, __global const uint* const ga, __local uint* la) {",
          b);

      // calculate g_offset as the index from which to start prefetching
      line("  const uint g_offset = sectionID * CELLS_PER_SECTION; ", b);

      // split the loop in two: first where we know every iteration has data; second where we need
      // to check bounds for these iterations we know that we are in bounds
      if (loopBoundFloor > 0) {
        line("  #pragma unroll LOOP_BOUND_FLOOR", b);
      }
      line(
          "  for (uint loop=0, cell=threadID; loop < LOOP_BOUND_FLOOR; loop++, cell += WORKGROUPSIZE) {",
          b);
      line("    const uint g_cell = g_offset + cell; ", b);
      line("    la[cell] = ga[g_cell]; ", b);
      line("  }", b);
      if (loopBoundFloor < loopBoundCeil) {
        // for these iterations we need to check that we are in bounds
        // there will be at most one iteration of this loop
        line("  #pragma unroll 1", b);
        line(
            "  for (uint loop=LOOP_BOUND_FLOOR, cell= WORKGROUPSIZE * LOOP_BOUND_FLOOR + threadID; loop < LOOP_BOUND_CEIL; loop++, cell += WORKGROUPSIZE) {",
            b);
        line("    const uint g_cell = g_offset + cell;", b);
        line("    if (cell < CELLS_PER_SECTION) { la[cell] = ga[g_cell]; }", b);
        line("  }", b);
      }
      line("  barrier(CLK_LOCAL_MEM_FENCE);", b);
      line("}", b);

      line(
          "void prefetch_2D_row_major_name(const uint tileID, const uint M, __global const uint* const ga, __local uint* la) {",
          b);

      line("const int row = get_local_id(0);", b);
      line("const int col = get_local_id(1);", b);
      line("const int globalRow = WORKGROUPSIZE * get_group_id(0) + row;", b);
      line("const int tiledCol = WORKGROUPSIZE * tileID + col;", b);
      line("la[col][row] = ga[tiledCol*M + globalRow];", b);
      line("}", b);

      line(
          "void prefetch_2D_col_major_name(const uint tileID, const uint K, __global const uint* const ga, __local uint* la) {",
          b);

      line("const int row = get_local_id(0);", b);
      line("const int col = get_local_id(1);", b);
      line("const int globalCol = WORKGROUPSIZE * get_group_id(1) + col;", b);
      line("const int tiledRow = WORKGROUPSIZE * tileID + row;", b);
      line("la[col][row] = ga[globalCol*K + tiledRow];", b);
      line("}", b);

    }

    final int actualIndicesPerSection = cellsPerSection * indicesPerCell;
    return b.toString().replace("name", name)
        .replace("INDICES_PER_SECTION", Integer.toString(actualIndicesPerSection))
        .replace("NUMBER_OF_SECTIONS", Integer.toString(sectionCount))
        .replace("CELLS_PER_SECTION", Integer.toString(cellsPerSection))
        .replace("WORKGROUPSIZE", Integer.toString(workgroupsize))
        .replace("LOOP_BOUND_FLOOR", Integer.toString(loopBoundFloor))
        .replace("LOOP_BOUND_CEIL", Integer.toString(loopBoundCeil))
        .replace("SCOPE", prefetch ? "__local" : "__global");

  }

  private static void line(final String s, final StringBuffer b) {
    b.append(s);
    b.append(System.lineSeparator());
  }

}
