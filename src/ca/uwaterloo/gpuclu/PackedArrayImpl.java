package ca.uwaterloo.gpuclu;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Stores multiple logical values in a single cell of an int array. "Index" refers to the logical
 * index of the value. "Cell" refers to the physical index, or to the bits in a cell.
 *
 */
public class PackedArrayImpl extends PackedArray {

  /**
   * Used to give anonymous PackedArrays unique names.
   */
  private final static AtomicInteger nextID = new AtomicInteger(0);

  /**
   * The data in this packed array.
   */
  private final int[] cells;

  private int highestSetIndex = -1;

  public final PackedArrayConfig config;

  /**
   * Construct a PackedArray with a generated name. The actual bitwidth will be a power of two that
   * is greater than or equal to the requested bitwidth. The actual logical capacity will be at
   * least as large as the requested capacity.
   *
   * @param requestedBitwidth
   * @param requestedCapacity
   */
  public PackedArrayImpl(final int requestedBitwidth, final int requestedCapacity) {
    this("PackedArray" + nextID.getAndIncrement(), requestedBitwidth, requestedCapacity);
  }

  public PackedArrayImpl(final String name, final int requestedBitwidth, final int requestedCapacity) {
    this(name, requestedBitwidth, 1, requestedCapacity);
  }

  public PackedArrayImpl(final String name, final int requestedBitwidth, final int sectionCount,
      final int indicesPerSection) {
    this(new PackedArrayConfig(name, requestedBitwidth, sectionCount, indicesPerSection));
  }

  public PackedArrayImpl(final PackedArrayConfig config) {
    this.config = config;
    // allocate the cells
    this.cells = new int[config.cellCount];
  }

  /** Recompute the highestSetIndex if the underlying array is directly edited **/
  public void recalibrateSize() {
    int i = 0;
    for (i = cells.length - 1; i >= 0; i--) {
      if (cells[i] != 0) {
        break;
      }
    }
    if (i == -1) {
      highestSetIndex = -1;
      return;
    }
    int index = (i + 1) * config.indicesPerCell - 1;
    while (get(index) == 0) {
      index--;
    }
    highestSetIndex = index;
  }

  /** The number of logical elements in this PackedArray. */
  @Override
  public int size() {
    return highestSetIndex + 1;
  }

  /** The logical capacity of this PackedArray. */
  @Override
  public int capacity() {
    return config.indexCount;
  }


  /**
   * The maximum value that can be stored in this packed array. If the bitwidth is 32 this will say
   * Integer.MAX_VALUE (which is only 31 bits).
   */
  @Override
  public int maxValue() {
    return (config.bitwidth < 32) ? config.valueMask : Integer.MAX_VALUE;
  }

  public int cellCount() {
    final int x = cellCount(config.indexCount);
    assert x == cells.length;
    return x;
  }

  public int cellCount(final int capacity) {
    return cellCount(config.indexCount, config.indicesPerCell);
  }

  public static int cellCount(final int capacity, final int valuesPerCell) {
    return (int) Math.ceil((float) capacity / (float) valuesPerCell);
  }

  protected int cell(final int index) {
    // these expressions are equivalent when bitwidth is a power of 2
    // return index / valuesPerCell;
    return index >> config.indicesPerCellLog2;
  }

  protected int subcell(final int index) {
    return (index & config.indexMask) << config.bitwidthLog2;
  }

  public void append(final int value) {
    set(highestSetIndex + 1, value);
  }

  @Override
  public void set(final int index, final int value) {
    // the mask for 32 bits (0xFFFFFFFF) is -1, so in that case we can't check value <= mask
    assert value <= config.valueMask || config.bitwidth == 32;
    final int cell = cell(index);
    assert cell < cells.length;
    final int subcell = subcell(index);
    set(cell, subcell, value);
    if (index > highestSetIndex) {
      highestSetIndex = index;
    }
  }

  private final void set(final int cell, final int subcell, final int value) {
    assert value <= config.valueMask || config.bitwidth == 32;
    cells[cell] &= ~(config.valueMask << subcell);
    cells[cell] |= value << subcell;
  }

  @Override
  public int get(final int index) {
    return get(cell(index), subcell(index));
  }

  private final int get(final int cell, final int subcell) {
    return (cells[cell] >>> subcell) & config.valueMask;
  }

  /**
   * Returns a reference to the underlying data. Clients should not mutate the data. Reference is
   * made available to facilitate serialization.
   */
  public final int[] getCells() {
    return cells;
  }

  public boolean hasLeftovers() {
    return (highestSetIndex >= 0) && (((highestSetIndex + 1) % config.indicesPerCell) != 0);
  }

  public PackedArrayImpl getLeftovers() {
    final int leftoverSize = ((highestSetIndex + 1) % config.indicesPerCell);
    final PackedArrayImpl leftovers =
        new PackedArrayImpl(config.name + "_leftovers", config.bitwidth, leftoverSize);
    for (int j = 0, i = highestSetIndex - leftoverSize + 1; i <= highestSetIndex; i++, j++) {
      // System.out.println("getLeftover:  " + Util.i2s(j, i));
      leftovers.set(j, get(i));
    }
    return leftovers;
  }

  public PackedArraySection section(final int section) {
    assert section < config.sectionCount;
    final int actualIndicesPerSection = config.cellsPerSection * config.indicesPerCell;
    final int startIndex = section * actualIndicesPerSection;
    return new PackedArraySection(this, startIndex, config.requestedIndicesPerSection, actualIndicesPerSection);
  }

}
