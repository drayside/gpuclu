// zero out an array of integers
__kernel void zero_byte(__global char* const a)
{
    const int tid = get_global_id(0);
    a[tid] = 0;
}