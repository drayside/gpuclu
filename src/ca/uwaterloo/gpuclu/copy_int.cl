// copy an array of integers
__kernel void copy_int(
	__global const int* const source, 
	__global int* const target, 
	const int length)
{
    const int tid = get_global_id(0);
    if (tid > length) return;
    target[tid] = source[tid];
}