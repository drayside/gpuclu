package ca.uwaterloo.gpuclu;

import org.jocl.CL;
import org.jocl.Pointer;
import org.jocl.Sizeof;
import org.jocl.cl_command_queue;
import org.jocl.cl_context;
import org.jocl.cl_event;
import org.jocl.cl_kernel;
import org.jocl.cl_mem;


public abstract class Mem {

  // metadata
  public final String name;
  public final MemConfig config;
  public final MemCapacity capacity;

  private int num_cells_uploaded = 0;

  // GPU data
  protected cl_mem d;

  // CPU/host data is stored in subclass


  public Mem(final String name, final MemConfig config, final MemCapacity capacity) {
    this.name = name;
    this.config = config;
    this.capacity = capacity;
    assert config.configOk();
  }

  public void init(final cl_context context) {
    // initialize on CPU
    initHost();
    // initialize mem on GPU
    final int d_capacity = deviceSizeUnit() * capacity.deviceCapacity();
    if (d_capacity > 0) {
      int[] err = OpenCLUtil.initOpenCLResult();
      d = CL.clCreateBuffer(context, config.device_access, d_capacity, null, err);
      OpenCLUtil.checkErrorOpenCL(err, "CL.clCreateBuffer: " + name);
    }
  }

  /** Initialize storage space on host (CPU). */
  public abstract void initHost();

  /**
   * Return the OpenCL constant that tells us whether this is an int-sized, byte-sized, etc, buffer.
   */
  public abstract int deviceSizeUnit();

  /** The current length of the buffer on host. */
  public abstract int h_length();

  /** Whether the host buffer exists. */
  public abstract boolean h_isNull();

  /** Zero out the host buffer (or just make a new one). */
  public abstract void h_clear();

  /**
   * Rectify any invariants that might have been violated by injecting an array from the GPU into
   * some data structure.
   */
  public void h_downloadPostprocessing() {
    return;
  }

  /** Zero out transient data on GPU. */
  public cl_event d_clear_transients(final cl_command_queue q, final boolean GPU_PROFILING) {
    if (config.persistent) {
      return null;
    }
    if (capacity.deviceCapacity() == 0) {
      return null;
    }
    if (config.upload || config.incremental_upload) {
      return null;
    }
    final cl_kernel k = d_zero_kernel();
    // clear out transient data on GPU
    // Set arguments for zeroing kernel
    int i = 0;
    OpenCLUtil.setKernelArg(k, "a", i++, Sizeof.cl_mem, d_pointer());

    // launch zeroing kernel
    // TODO: GPU profiling of zero kernels
    final cl_event e = GPU_PROFILING ? new cl_event() : null;
    final int work_units = capacity.deviceCapacity();
    final int[] err = OpenCLUtil.initOpenCLResult();
    err[0] = CL.clEnqueueNDRangeKernel(q, k, 1, null, new long[] {work_units}, null, 0, null, e);
    OpenCLUtil.checkErrorOpenCL(err, "clEnqueueNDRangeKernel");
    return e;
  }

  public abstract cl_kernel d_zero_kernel();

  /** OpenCL pointer to the host buffer. */
  public abstract Pointer h_pointer();

  /** OpenCL pointer to the device buffer. */
  public Pointer d_pointer() {
    return Pointer.to(d);
  };

  /** Number of cells (ints, bytes, etc) uploaded so far. */
  public int num_cells_uploaded() {
    return num_cells_uploaded;
  }

  /**
   * Upload the contents of h to d. If this is an incremental Mem, then append the contents of h at
   * the end of d. Otherwise, overwrite the existing contents of d. The host buffer is released
   * after upload.
   *
   * @param command_queue
   * @param GPU_PROFILING
   * @return cl_event if GPU_PROFILING is true and there is data to upload; null otherwise.
   */
  public cl_event upload(cl_command_queue command_queue, final boolean GPU_PROFILING) {
    // safety checks
    if (h_isNull() || config.upload == false) {
      // nothing to upload
      return null;
    }
    if (config.incremental_upload) {
      // do we have space to append?
      assert h_length() <= (capacity.deviceCapacity() - d_offset()) : "not enough capacity on device to append "
          + name + Util.i2s(h_length(), capacity.deviceCapacity(), num_cells_uploaded);
    } else {
      // do we have space to upload?
      assert h_length() <= capacity.deviceCapacity() : "not enough capacity on device to upload "
          + name + " " + Util.i2s(h_length(), capacity.deviceCapacity());
    }
    // check if there is anything in the host buffer to upload
    final int size = h_length() * deviceSizeUnit();
    if (size == 0) {
      return null;
    }
    // System.out.println(name + " up: " + h_length());
    // do it
    final cl_event event = GPU_PROFILING ? new cl_event() : null;
    final int[] err = OpenCLUtil.initOpenCLResult();
    final int d_offset = d_offset() * deviceSizeUnit();
    err[0] =
        CL.clEnqueueWriteBuffer(command_queue, d, true, d_offset, size, h_pointer(), 0, null, event);
    OpenCLUtil.checkErrorOpenCL(err, "clEnqueueWriteBuffer: " + name);
    num_cells_uploaded += h_length();

    // release the host buffer
    releaseHost();

    // return the event object (which might be null if GPU_PROFILING is off)
    return event;
  }

  /**
   * Offset on the device for next upload. Only needs to be overridden by subclasses doing some kind
   * of packing. Not intended to be called by clients.
   */
  protected int d_offset() {
    return config.incremental_upload ? num_cells_uploaded : 0;
  }

  /**
   *
   * @param command_queue
   * @param GPU_PROFILING
   * @return cl_event if GPU_PROFILING is true and host buffer is not null; null otherwise.
   */
  public cl_event download(cl_command_queue command_queue, final boolean GPU_PROFILING) {
    // safety checks
    if (!config.download) {
      return null;
    }
    if (capacity.deviceCapacity() == 0) {
      return null;
    }

    if (h_isNull()) {
      initHost();
    }

    // size of the entire buffer on GPU
    // final int size = capacity.deviceCapacity() * deviceSizeUnit();

    // download only what will fit in our host buffer
    final int size = h_length() * deviceSizeUnit();
    // do it
    final cl_event event = GPU_PROFILING ? new cl_event() : null;
    final int[] err = OpenCLUtil.initOpenCLResult();
    err[0] = CL.clEnqueueReadBuffer(command_queue, d, true, 0, size, h_pointer(), 0, null, event);
    OpenCLUtil.checkErrorOpenCL(err, "clEnqueueReadBuffer: " + name);
    return event;
  }

  /**
   * Any pre-processor directives or accessor methods necessary to use this Mem in OpenCL code.
   */
  public String getOpenCLCode() {
    return "";
  }

  public void release() {
    // release on CPU
    releaseHost();
    // release on GPU
    int[] err = OpenCLUtil.initOpenCLResult();
    err[0] = CL.clReleaseMemObject(d);
    OpenCLUtil.checkErrorOpenCL(err, "CL.clReleaseMemObject: " + name);
    d = null;
  }

  public abstract void releaseHost();
}
