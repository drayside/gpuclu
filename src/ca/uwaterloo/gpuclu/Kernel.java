package ca.uwaterloo.gpuclu;

import java.util.ArrayList;
import java.util.List;

import org.jocl.Pointer;
import org.jocl.Sizeof;
import org.jocl.cl_context;
import org.jocl.cl_event;
import org.jocl.cl_kernel;
import org.jocl.cl_program;

public abstract class Kernel {

  public final String fileName;
  public final String functionName;
  public final boolean gpu_profiling;
  cl_program program;
  protected cl_kernel kernel;
  protected long total_work_items = 0;
  private int argIndex = 0;

  protected long maxWorkgroupSize;
  protected long wavefrontSize;

  // kernel launch parameters
  protected int work_dim = 1;
  protected long[] global_work_offset = null;
  protected long[] global_work_size = null;
  protected long[] local_work_size = null;
  protected int num_events_in_wait_list = 0;
  protected cl_event[] event_wait_list = null;
  protected cl_event event = null;

  public Kernel(final String fileName, final String functionName) {
    this(fileName, functionName, true);
  }

  public Kernel(final String fileName, final String functionName, final boolean gpu_profiling) {
    super();
    this.fileName = fileName;
    this.functionName = functionName;
    this.gpu_profiling = gpu_profiling;
  }

  public void init(final cl_context context, final long maxWorkgroupSize, final long wavefrontSize) {
    init(context, maxWorkgroupSize, wavefrontSize, new Mem[0]);
  }

  public void init(final cl_context context, final long maxWorkgroupSize, final long wavefrontSize,
      final Mem[] memories) {
    this.maxWorkgroupSize = maxWorkgroupSize;
    this.wavefrontSize = wavefrontSize;
    final List<String> prefix = preprocessorDirectives();
    for (final Mem m : memories) {
      prefix.add(m.getOpenCLCode());
    }
    program = OpenCLUtil.initProgram(fileName, context, prefix);
    kernel = OpenCLUtil.initKernel(program, functionName);
  }

  /**
   * May be overridden by subclasses. Subclasses should start their implementation by calling this
   * method.
   */
  protected List<String> preprocessorDirectives() {
    final List<String> p = new ArrayList<String>();
    if (gpu_profiling) {
      p.add("#define GPU_PROFILING");
    }
    if (Util.areAssertionsEnabled()) {
      p.add("#define ASSERTIONS");
    }
    // TODO: This define is GPU engine specific, and should be refactored out of here.
    p.add("#define WORKGROUPSIZE " + wavefrontSize);
    return p;
  }

  public void release() {
    OpenCLUtil.releaseKernel(kernel, fileName);
    OpenCLUtil.releaseProgram(program, fileName);
  }

  public void setArg(final Mem m) {
    setArg(m.name, Sizeof.cl_mem, m.d_pointer());
  }

  public void setArg(final String name, final int size, final Pointer p) {
    OpenCLUtil.setKernelArg(kernel, name, argIndex, size, p);
    argIndex++;
  }

  public final cl_event prepareLaunch() {
    // launch
    final cl_event result = prepareLaunchInner();
    // reset argIndex for next launch
    argIndex = 0;
    // return result
    return result;
  }

  public abstract cl_event prepareLaunchInner();

  protected long fitToWorkgroup(final int workItems) {
    if (workItems == 1)
      return workItems;
    else
      return wavefrontSize * (long) Math.ceil((double) workItems / (double) wavefrontSize);
  }
}


class ZeroKernel extends Kernel {

  public ZeroKernel(final String fileName, final String functionName) {
    super(fileName, functionName);
  }

  /**
   * ZeroKernels are launched by Mem, not here.
   *
   * @see Mem#d_clear_transients(org.jocl.cl_command_queue, boolean)
   */
  @Override
  public cl_event prepareLaunchInner() {
    throw new UnsupportedOperationException("Zero kernels are launched by Mem.d_clear_transients()");
  }

}
