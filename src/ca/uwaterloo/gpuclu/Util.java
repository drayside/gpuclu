package ca.uwaterloo.gpuclu;

import java.util.List;

public final class Util {

  private Util() {
    throw new UnsupportedOperationException("static utility methods only");
  }

  public static String i2s(final Integer... integers) {
    final StringBuilder b = new StringBuilder();
    for (final Integer i : integers) {
      b.append(i);
      b.append(' ');
    }
    return b.toString();
  }

  public static int[] list2array(final List<Integer> list) {
    if (list == null) {
      return new int[0];
    }
    final int[] result = new int[list.size()];
    int i = 0;
    for (final Integer x : list) {
      result[i] = x;
      i++;
    }
    return result;
  }

  /** Used to run more complex blocks of code when assertions are enabled. */
  public static boolean areAssertionsEnabled() {
    boolean x = false;
    assert x = true : "the assignment will always succeed, but will only be executed if assertions are enabled";
    return x;
  }

}
