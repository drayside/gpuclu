package ca.uwaterloo.gpuclu;

import org.jocl.CL;
import org.jocl.cl_command_queue;
import org.jocl.cl_context;
import org.jocl.cl_context_properties;
import org.jocl.cl_device_id;
import org.jocl.cl_event;
import org.jocl.cl_platform_id;

public final class OpenCLComputer {

  public final boolean GPU_PROFILING;

  public final boolean ASSERTIONS;

  /**
   * GPU profiling stats. Null if GPU_PROFILING is off. First index is Upload. Last index is
   * Download. Intermediate indices are for kernels. This should be final, but cannot be initialized
   * in AbstractGPUEngine constructor because the kernels array is not yet initialized at that time.
   */
  private DurationSummary[] durationSummaries;
  private final static int UPLOAD_INDEX = 0;
  private int DOWNLOAD_INDEX = -1;

  /** File with kernel for zeroing out an array of integers. */
  private final static String kernel_file_zero_int = "src/ca/uwaterloo/gpuclu/zero_int.cl";

  /** File with kernel for zeroing out an array of bytes. */
  private final static String kernel_file_zero_byte = "src/ca/uwaterloo/gpuclu/zero_byte.cl";



  // OpenCL resources: There are six essential resources that every OpenCL program must manage.
  // These are: platforms, devices, contexts, command queues, programs, and kernels.

  private cl_platform_id[] platforms;

  private cl_device_id device;

  private cl_context context;

  private cl_command_queue command_queue;

  /** Zero kernels are used to zero out arrays on the GPU. The order of these is hard-coded. */
  private final ZeroKernel[] zero_kernels = new ZeroKernel[] {
      new ZeroKernel(kernel_file_zero_int, "zero_int"),
      new ZeroKernel(kernel_file_zero_byte, "zero_byte")};

  /** Width of text fields in the console log. */
  protected final int LOG_WIDTH;

  private int cycleCount = 0;

  private final OpenCLComputation computation;

  private Thread zero_thread = null;

  public OpenCLComputer(final boolean GPU_PROFILING, final boolean ASSERTIONS,
      final OpenCLComputation c) {
    this(GPU_PROFILING, ASSERTIONS, LogHelper.LOG_WIDTH, c);
  }

  public OpenCLComputer(final boolean GPU_PROFILING, final boolean ASSERTIONS, final int logWidth,
      final OpenCLComputation c) {
    super();
    this.GPU_PROFILING = GPU_PROFILING;
    this.ASSERTIONS = ASSERTIONS;
    this.LOG_WIDTH = logWidth;
    this.computation = c;
  }

  /**
   * This method allocates and initializes OpenCL resources. This includes identifying an available
   * device and setting it up for compute. The resources to be initialized include: (1) platform,
   * (2) device, (3) context, (4) command queue, (5) program, (6) and kernel.
   */
  public void initialize() {
    this.DOWNLOAD_INDEX = computation.kernels().length + 1;
    final int[] err = OpenCLUtil.initOpenCLResult();
    platforms = OpenCLUtil.getPlatforms();
    final OpenCLUtil.GPUDeviceResult result = OpenCLUtil.getGPUDevice(platforms);
    final cl_platform_id platform = result.platform;
    device = result.device;

    System.out.println("Using GPU: " + OpenCLUtil.getString(device, CL.CL_DEVICE_NAME));

    // Initialize the context properties
    final cl_context_properties contextProperties = new cl_context_properties();
    contextProperties.addProperty(CL.CL_CONTEXT_PLATFORM, platform);

    // Create a context for the selected device
    context =
        CL.clCreateContext(contextProperties, 1, new cl_device_id[] {device}, null, null, err);
    OpenCLUtil.checkErrorOpenCL(err, "CL.clCreateContext");

    // Create a command-queue for the selected device
    final long profiling = GPU_PROFILING ? CL.CL_QUEUE_PROFILING_ENABLE : 0;
    final long qflags = profiling;
    // we have not set the out-of-order flag, so we expect kernels to execute in order
    // CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE
    command_queue = CL.clCreateCommandQueue(context, device, qflags, err);
    OpenCLUtil.checkErrorOpenCL(err, "CL.clCreateCommandQueue");

    // initialize programs and kernels
    final long maxWorkgroupSize = OpenCLUtil.getMaxWorkgroupSize(device);
    final long wavefrontSize = OpenCLUtil.getWavefrontSize(device);
    for (int i = 0; i < computation.kernels().length; i++) {
      computation.kernels()[i].init(context, maxWorkgroupSize, wavefrontSize,
          computation.memories());
    }
    for (int i = 0; i < zero_kernels.length; i++) {
      zero_kernels[i].init(context, maxWorkgroupSize, wavefrontSize);
    }

    // hard-coded array indices
    MemInt.zero_kernel = zero_kernels[0].kernel;
    MemByte.zero_kernel = zero_kernels[1].kernel;
    MemPacked.zero_kernel = zero_kernels[0].kernel;

    // initialize memories
    for (final Mem m : computation.memories()) {
      m.init(context);
      // clear out state that might be persisting on GPU from previous executions
      m.d_clear_transients(command_queue, GPU_PROFILING);
    }

    // initialize performance counter storage
    if (GPU_PROFILING) {
      // initialize GPU profiling summary data structures
      // one for each kernel, plus upload and download
      final Kernel[] a = computation.kernels();
      durationSummaries = new DurationSummary[a.length + 2];
      durationSummaries[0] =
          new DurationSummary("Upload", computation.logger(), LogHelper.LOG_WIDTH);
      durationSummaries[a.length + 1] =
          new DurationSummary("Download", computation.logger(), LOG_WIDTH);
      for (int i = 0; i < a.length; i++) {
        durationSummaries[i + 1] =
            new DurationSummary(a[i].functionName, computation.logger(), LogHelper.LOG_WIDTH);
      }
    } else {
      durationSummaries = null;
    }

  }

  /**
   * This method copies host (CPU) memory to an OpenCL device (usually, a GPU).
   *
   * @param num_explorers the number of explorers whose information is to be copied
   * @param stat_block_size the number of entries in the statistics array to be copied
   */
  private void upload() {

    computation.uploadPreprocessing();

    // upload all memories
    final cl_event[] events = new cl_event[computation.memories().length];
    for (int i = 0; i < computation.memories().length; i++) {
      events[i] = computation.memories()[i].upload(command_queue, GPU_PROFILING);
    }
    // we do not wait for upload to complete here
    // the kernels get added to the command queue and it all goes together


    // record profiling stats
    if (GPU_PROFILING) {
      final ExecutionStatistics es = new ExecutionStatistics(durationSummaries[UPLOAD_INDEX]);
      for (int i = 0; i < computation.memories().length; i++) {
        es.addEntry(computation.memories()[i].name, events[i]);
      }
    }

    computation.uploadPostprocessing();
  }

  /**
   * Copies device (GPU) memory to host (CPU) memory. This method copies the query results from
   * device to host. The query results include the result type, clause id(s), and statistics.
   */
  private void download() {

    computation.downloadPreprocessing();

    // download all memories (some memories will choose to not download themselves)
    final cl_event[] events = new cl_event[computation.memories().length];
    for (int i = 0; i < computation.memories().length; i++) {
      events[i] = computation.memories()[i].download(command_queue, GPU_PROFILING);
    }

    // wait for download to complete
    CL.clFinish(command_queue);

    // clear out transient data on GPU, asynchronously
    zero_thread = new Thread() {
      @Override
      public void run() {
        for (final Mem m : computation.memories()) {
          m.d_clear_transients(command_queue, GPU_PROFILING);
        }
        // wait for clearing to complete
        CL.clFinish(command_queue);
      }
    };
    zero_thread.run();

    // record profiling stats
    if (GPU_PROFILING) {
      final ExecutionStatistics es = new ExecutionStatistics(durationSummaries[DOWNLOAD_INDEX]);
      for (int i = 0; i < computation.memories().length; i++) {
        es.addEntry(computation.memories()[i].name, events[i]);
      }
    }

    // post-processing
    // generic post-processing to rectify invariants
    for (final Mem m : computation.memories()) {
      m.h_downloadPostprocessing();
    }
    // computation-specific post-processing
    computation.downloadPostprocessing();
  }

  /**
   * Uploads memories to GPU, runs kernels, downloads results.
   */
  public void compute() {

    // wait for zero thread to finish clearing GPU transient state
    if (zero_thread != null) {
      try {
        zero_thread.join();
      } catch (final InterruptedException e) {
        e.printStackTrace();
        throw new RuntimeException(e);
      }
    }

    // Copy host memory to device
    upload();

    // launch kernels
    final cl_event[] launch_events = new cl_event[computation.kernels().length];
    for (int i = 0; i < launch_events.length; i++) {
      // OpenCL default configuration is for each kernel to finish before the next one starts
      // so we do not need to block in between kernel launches here.
      final Kernel k = computation.kernels()[i];
      launch_events[i] = k.prepareLaunch(); // rename this to prepareLaunch()
      final int[] err = OpenCLUtil.initOpenCLResult();
      err[0] =
          CL.clEnqueueNDRangeKernel(command_queue, k.kernel, k.work_dim, k.global_work_offset,
              k.global_work_size, k.local_work_size, k.num_events_in_wait_list, k.event_wait_list,
              k.event);
      OpenCLUtil.checkErrorOpenCL(err, "clEnqueueNDRangeKernel");

    }

    // block until all GPU commands have completed
    CL.clFinish(command_queue);
    cycleCount++;

    // clear host memories for next cycle
    for (final Mem m : computation.memories()) {
      m.h_clear();
    }

    // Copy result from device to host
    download();

    // record execution statistics
    if (GPU_PROFILING) {
      for (int i = 0; i < launch_events.length; i++) {
        final ExecutionStatistics es = new ExecutionStatistics(durationSummaries[i + 1]);
        es.addEntry(computation.kernels()[i].functionName, launch_events[i]);
        CL.clReleaseEvent(launch_events[i]);
      }
    }
  }

  /**
   * This method releases OpenCL-related resources. These include memory objects (variables with the
   * d_ prefix), and OpenCL-related data structures (command queue, kernel, program, and context).
   */
  public void release() {

    if (GPU_PROFILING) {
      final LogHelper lh = new LogHelper(computation.logger(), LOG_WIDTH);
      for (final Kernel k : computation.kernels()) {
        lh.info(k.functionName + " work items", k.total_work_items, false);
      }
      for (final DurationSummary d : durationSummaries) {
        d.log(cycleCount);
      }
    }

    // The calls to "clRelease" functions will free/release OpenCL resources and device memory.

    // release memories
    for (final Mem m : computation.memories()) {
      m.release();
    }

    // release OpenCL resources

    // release kernels and programs
    MemInt.zero_kernel = null;
    MemByte.zero_kernel = null;
    MemPacked.zero_kernel = null;
    for (final Kernel k : computation.kernels()) {
      k.release();
    }
    for (final Kernel k : zero_kernels) {
      k.release();
    }

    final int[] err = OpenCLUtil.initOpenCLResult();
    err[0] = CL.clReleaseCommandQueue(command_queue);
    OpenCLUtil.checkErrorOpenCL(err, "clReleaseCommandQueue");

    err[0] = CL.clReleaseContext(context);
    OpenCLUtil.checkErrorOpenCL(err, "clReleaseContext");

    // This causes a JVM crash on my GPU, so skip it as it's not strictly necessary.
    final String version = OpenCLUtil.getString(device, CL.CL_DEVICE_OPENCL_C_VERSION);
    if (!version.startsWith("OpenCL C 1.1")) {
      err[0] = CL.clReleaseDevice(device);
      OpenCLUtil.checkErrorOpenCL(err, "clReleaseDevice (id=" + 0 + ")");
    }

  }

}
