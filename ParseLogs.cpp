#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#define NUMBER_OF_ITERATIONS 10;
using namespace std;

void parseValues(string filePrefix, int fileCount, float &uq, float &ul, float &uc, 
                    float &kq, float &kl, float &kc, float &dq, float &dl, float &dc)
{
    float value;
    string line, word, process, statistic, valueString;
    for (int fileIndex = 0; fileIndex < fileCount; fileIndex++) {
        string fileName = filePrefix;
        fileName.append(to_string(fileIndex));
        fileName.append(".txt");
        ifstream infile(fileName);
        while(getline(infile, line))
        {
            std::size_t findMessage = line.find("<message>");
            if(findMessage == string::npos)
                continue;
            if(line.find("work items:") != string::npos)
                continue;
            line.replace(findMessage, 9, "<message>    ");
            istringstream stream(line);
            stream >> word;
            stream >> process;
            stream >> statistic;
            stream >> valueString;
            value = stof(valueString);
            if (process.compare("Upload") == 0) {
                if (statistic.compare("queued:") == 0) {
                    uq += value;
                } else if (statistic.compare("latency:") == 0) {
                    ul += value;
                } else {
                    uc += value;
                }
            } else if (process.compare("Download") == 0) {
                if (statistic.compare("queued:") == 0) {
                    dq += value;
                } else if (statistic.compare("latency:") == 0) {
                    dl += value;
                } else {
                    dc += value;
                    goto X;
                }
            } else {
                if (statistic.compare("queued:") == 0) {
                    kq += value;
                } else if (statistic.compare("latency:") == 0) {
                    kl += value;
                } else {
                    kc += value;
                }
            }
        }
        X:    infile.close();
    }
    uq /= fileCount;
    ul /= fileCount;
    uc /= fileCount;
    kq /= fileCount;
    kl /= fileCount;
    kc /= fileCount;
    dq /= fileCount;
    dl /= fileCount;
    dc /= fileCount;
    string outfileName = filePrefix;
    outfileName.append("_stats.txt");
    ofstream outfile(outfileName);
    outfile<<uq<<" "<<ul<<" "<<uc<<" "<<kq<<" "<<kl<<" "<<kc<<" "<<uq<<" "<<ul<<" "<<uc<<endl;
    outfile.close();
}

int main() {
    int fileCount = NUMBER_OF_ITERATIONS;
    float prefetch_uq = 0, prefetch_ul = 0, prefetch_uc = 0, prefetch_kq = 0,
                prefetch_kl = 0, prefetch_kc = 0, prefetch_dq = 0, prefetch_dl = 0, prefetch_dc = 0;
    float no_prefetch_uq = 0, no_prefetch_ul = 0, no_prefetch_uc = 0, no_prefetch_kq = 0,
                no_prefetch_kl = 0, no_prefetch_kc = 0, no_prefetch_dq = 0, no_prefetch_dl = 0, no_prefetch_dc = 0;
    float packed_sum_uq = 0, packed_sum_ul = 0, packed_sum_uc = 0, packed_sum_kq = 0,
                packed_sum_kl = 0, packed_sum_kc = 0, packed_sum_dq = 0, packed_sum_dl = 0, packed_sum_dc = 0;
    float unpacked_sum_uq = 0, unpacked_sum_ul = 0, unpacked_sum_uc = 0, unpacked_sum_kq = 0,
                unpacked_sum_kl = 0, unpacked_sum_kc = 0, unpacked_sum_dq = 0, unpacked_sum_dl = 0, unpacked_sum_dc = 0;
    float packed_sum_byte_uq = 0, packed_sum_byte_ul = 0, packed_sum_byte_uc = 0, packed_sum_byte_kq = 0,
                packed_sum_byte_kl = 0, packed_sum_byte_kc = 0, packed_sum_byte_dq = 0, packed_sum_byte_dl = 0, packed_sum_byte_dc = 0;
    float unpacked_sum_byte_uq = 0, unpacked_sum_byte_ul = 0, unpacked_sum_byte_uc = 0, unpacked_sum_byte_kq = 0,
                unpacked_sum_byte_kl = 0, unpacked_sum_byte_kc = 0, unpacked_sum_byte_dq = 0, unpacked_sum_byte_dl = 0, unpacked_sum_byte_dc = 0;
    float matrix_sum_xy_uq = 0, matrix_sum_xy_ul = 0, matrix_sum_xy_uc = 0, matrix_sum_xy_kq = 0,
                matrix_sum_xy_kl = 0, matrix_sum_xy_kc = 0, matrix_sum_xy_dq = 0, matrix_sum_xy_dl = 0, matrix_sum_xy_dc = 0;
    float matrix_sum_yx_uq = 0, matrix_sum_yx_ul = 0, matrix_sum_yx_uc = 0, matrix_sum_yx_kq = 0,
                matrix_sum_yx_kl = 0, matrix_sum_yx_kc = 0, matrix_sum_yx_dq = 0, matrix_sum_yx_dl = 0, matrix_sum_yx_dc = 0;
    parseValues("prefetch_log", fileCount, prefetch_uq, prefetch_ul, prefetch_uc, 
                                    prefetch_kq, prefetch_kl, prefetch_kc, prefetch_dq, prefetch_dl, prefetch_dc);
    parseValues("no_prefetch_log", fileCount, no_prefetch_uq, no_prefetch_ul, no_prefetch_uc, 
                                    no_prefetch_kq, no_prefetch_kl, no_prefetch_kc, no_prefetch_dq, no_prefetch_dl, no_prefetch_dc);
    parseValues("packed_sum_log", fileCount, packed_sum_uq, packed_sum_ul, packed_sum_uc,
            packed_sum_kq, packed_sum_kl, packed_sum_kc, packed_sum_dq, packed_sum_dl, packed_sum_dc);
    parseValues("unpacked_sum_log", fileCount, unpacked_sum_uq, unpacked_sum_ul, unpacked_sum_uc,
            unpacked_sum_kq, unpacked_sum_kl, unpacked_sum_kc, unpacked_sum_dq, unpacked_sum_dl, unpacked_sum_dc);
    parseValues("packed_sum_byte_log", fileCount, packed_sum_byte_uq, packed_sum_byte_ul, packed_sum_byte_uc,
            packed_sum_byte_kq, packed_sum_byte_kl, packed_sum_byte_kc, packed_sum_byte_dq, packed_sum_byte_dl, packed_sum_byte_dc);
    parseValues("unpacked_sum_byte_log", fileCount, unpacked_sum_byte_uq, unpacked_sum_byte_ul, unpacked_sum_byte_uc,
            unpacked_sum_byte_kq, unpacked_sum_byte_kl, unpacked_sum_byte_kc, unpacked_sum_byte_dq, unpacked_sum_byte_dl, unpacked_sum_byte_dc);
    parseValues("matrix_copy_xy_log", fileCount, matrix_sum_xy_uq, matrix_sum_xy_ul, matrix_sum_xy_uc,
            matrix_sum_xy_kq, matrix_sum_xy_kl, matrix_sum_xy_kc, matrix_sum_xy_dq, matrix_sum_xy_dl, matrix_sum_xy_dc);
    parseValues("matrix_copy_yx_log", fileCount, matrix_sum_yx_uq, matrix_sum_yx_ul, matrix_sum_yx_uc,
            matrix_sum_yx_kq, matrix_sum_yx_kl, matrix_sum_yx_kc, matrix_sum_yx_dq, matrix_sum_yx_dl, matrix_sum_yx_dc);
}
