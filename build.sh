#!/bin/bash
javac \
    -d bin/ \
    -sourcepath src/ \
    -classpath lib/JOCL-0.1.9.jar:lib/jcommander-1.48.jar:/usr/share/java/junit4.jar \
    `find src/  -type f -name '*.java'` \
    `find test/ -type f -name '*.java'` 
